
var tableBody = document.getElementById('tbody'); // getting the table data
var handler = "handler/tableUpdate.php";

tableBody.onclick = function (e) {
    var classDelete = e.target.className;
    var fileName = e.target.id;  //identifying the id of clicked cell

    if (classDelete == "delete") {
        xmlhttp = new XMLHttpRequest();
        var fd = new FormData();
        fd.append("fileName", fileName);
        fd.append("delete", "delete");
        fd.append("tpl", "table_loop.tpl");

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                document.getElementById("tbody").innerHTML = xmlhttp.responseText;
                undateStatData();
                undateUsedVolume();
            }
        }
        xmlhttp.open("POST", handler, true);
        xmlhttp.send(fd);
    }
};


function uploadFile() {
    var fileInput = document.getElementById("userfile");
    var file = fileInput.files[0];
    var fd = new FormData();
    fd.append("userfile", file);
    fd.append("tpl", "table_loop.tpl");
    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            document.getElementById("tbody").innerHTML = xmlhttp.responseText;
            document.getElementById("userfile").value = "";
            undateStatData();
            undateUsedVolume();
        }
    }
    xmlhttp.open("POST", handler, true);
    xmlhttp.send(fd);
};

function downloadFile() {
    var url = document.getElementById("url-input").value;
    var checked = document.getElementById("download").checked;
    var fd = new FormData();
    fd.append("url", url);
    fd.append("checked", checked);
    fd.append("tpl", "table_loop.tpl");

    xmlhttp=new XMLHttpRequest();
	if(checked == true) {  // to download file to server
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	
				document.getElementById("tbody").innerHTML = xmlhttp.responseText;
				document.getElementById("url-input").value = "";
				undateStatData();
				undateUsedVolume();
			}
		}		
	}
	else if (checked == false) {
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) { //to download file to pc we creating a url and click it by js
				var a = document.createElement("a");
				a.style = "display: none";
				document.body.appendChild(a);
				a.href = "/php04_zimnitskaya/task6/2/downloads/"+xmlhttp.responseText;
				a.download = xmlhttp.responseText;
				a.click();
			}
		}		
	}

    xmlhttp.open("POST", handler, true);
    xmlhttp.send(fd);
};

//updating stat data
function undateStatData() {
    var sd = new FormData();
    sd.append("tpl", "statistic.tpl");
    xhrS=new XMLHttpRequest();
    xhrS.onreadystatechange = function() {
        if (xhrS.readyState==4 && xhrS.status==200) {
            document.getElementById("information").innerHTML = xhrS.responseText;
        }
    }
    xhrS.open("POST", handler, true);
    xhrS.send(sd);
}

//update the user used volume in the directory
function undateUsedVolume() {
    var uv = new FormData();
    uv.append("tpl", "used_volume.tpl");
    xhr=new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState==4 && xhr.status==200) {
            document.getElementById("used_volume").innerHTML = xhr.responseText;
        }
    }
    xhr.open("POST", handler, true);
    xhr.send(uv);
}