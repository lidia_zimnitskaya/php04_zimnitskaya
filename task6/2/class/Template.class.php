<?php

class Template
{
    private $page;
    private $templates_dir = "templates/";
    private $app_parameters ;
    private $stat_data;
    private $user_permits;
    private $user_data;
    private $user_files;

    function __construct($tpl_file, $app_parameters, $stat_data, $user_permits, $user_data, $user_files)
    {
        if (!is_file($this->templates_dir.$tpl_file)) {
            throw new Exception('Main template [' . $tpl_file . '] not found!');
        }
        $this->page = file_get_contents($this->templates_dir.$tpl_file);
        $this->app_parameters = $app_parameters;
        $this->stat_data = $stat_data;
        $this->user_permits = $user_permits;
        $this->user_data = $user_data;
        $this->user_files = $user_files;
    }

    public function read_tpl_file($tpl_file) : string
    {
        $tpl_full_path = $this->templates_dir.$tpl_file[1];
        if (!is_file($tpl_full_path)) {
            throw new Exception('Main template [' . $tpl_file . '] not found!');
        }
        else {
            return file_get_contents($tpl_full_path);
        }
    }

    public function process_db_vars($var) : string
    {
        $db_var = $var [1];
        preg_replace("/{DB=\"([\w\.]+)\"}/", $this->app_parameters[$db_var] ,$this->page);
        return $this->app_parameters[$db_var];
    }

    public function process_stat_vars($var) : int
    {
        $db_var = $var [1];
        preg_replace("/{DB=\"([\w\.]+)\"}/", $this->stat_data[$db_var] ,$this->page);
        return $this->stat_data[$db_var];
    }

    public function process_dymanic_vars($var) // :?string - not declarating type null or string, it doesn't work in 7.0, works in 7.1
    {
        $db_var = $var [1];
        if($db_var == 'footer_date' && $this->app_parameters['start_year'] !== date('Y')) {
            $replacement = '&ndash; ' .date('Y');
            preg_replace("/{DV=\"([\w\.\_]+)\"}/", $replacement  , $this->page);
        }
        return $replacement;
    }

    public function proccess_loops($var) //: string
    {
        $db_var = $var[1];  // getting the name of an array which contains the necessary data. We put that name in the placeholder {LOOP=user_permits="files_permissions.tpl"}
        $tpl_file = $this->templates_dir.$var[2];  // the path where the template is. ALso take it from the placeholder.
        $count = count($this->$db_var); // count how many times we need to upload the loop template

        for ($i=0; $i<$count; $i++){   // start the loop which will handle the ultimate template
            $text .= file_get_contents($tpl_file);  // get content from the template
            $pattern = "/{LD=\"([\w\.\_]+)\"}/";
            preg_match_all ($pattern, $text, $matches); // looking for the matches first and i+ times
            $count_m = count($matches[1]);  // count how many matches (vars) we get each time
            for ($j=0; $j<$count_m; $j++) { // start a new loop to replace each placeholder with the an appropriate array data
                $v = $matches[1][$j];
                $replacement = $this->$db_var[$i][$v];
                $text = preg_replace("/{LD=\"([\w\.\_]+)\"}/", $replacement  , $text, $limit = 1);  // replace just one match in a time
            }
        }
        return $text;
    }

    public function proccess_user_data($var) : string
    {
        $db_var = $var [1];
        preg_replace("/{UD=\"([\w\.]+)\"}/", $this->user_data[$db_var] ,$this->page);
        return $this->user_data[$db_var];
    }

    public function proccess_tpl_files()
    {
        while (preg_match("/{FILE=\"[\w\.\_]+\"}/", $this->page)) {
            $this->page = preg_replace_callback("/{FILE=\"([\w\.\_]+)\"}/", array($this, 'read_tpl_file'), $this->page);
        }
        $this->page = preg_replace_callback("/{DB=\"([\w\.\_]+)\"}/", array($this, 'process_db_vars'), $this->page);
        $this->page = preg_replace_callback("/{SV=\"([\w\.\_]+)\"}/", array($this, 'process_stat_vars'), $this->page);
        $this->page = preg_replace_callback("/{DV=\"([\w\.\_]+)\"}/", array($this, 'process_dymanic_vars'), $this->page);
        $this->page = preg_replace_callback("/{UD=\"([\w\.\_]+)\"}/", array($this, 'proccess_user_data'), $this->page);
        $this->page = preg_replace_callback("/{LOOP=([\w\.\_]+)=\"([\w\.\_]+)\"}/", array($this, 'proccess_loops'), $this->page);
    }

    public function get_file_contents() : string
    {
       return $this->page;
    }
}
