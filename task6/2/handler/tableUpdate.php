<?php

include_once('../config.php');
include_once('../class/DB.class.php');
include_once('../class/Template.class.php');
//include_once('../class/User.class.php');
include_once('../class/File.class.php');

if($_SESSION['user_session']) {
    $id = $_SESSION['user_session'];
}
else {
    $id = $_COOKIE['Remember'];
}

$tr = $_POST['tpl'];
$dbXML = new DB($DB_con);
//$user_files = $dbXML-> get_user_files($id); // creating an array that contains info about user files
$app_parameters = $dbXML -> get_application_parameters() ; // creating an array that contains text info about our application
$stat_data = $dbXML -> get_stat_data(); // creating an array that contains statistic info about our application
$user_permits = $dbXML-> get_user_permits($id); // creating an array that contains info about logged in user permissions
$user_data = $dbXML-> get_user_info($id); // creating an array that contains user info

$file = new FILE($DB_con, $id, $user_permits, $user_data);

if ($_POST['url']!='' && $_POST['checked'] == "false") {
	echo $file -> download_to_pc();
}
else {
	
if($_FILES['userfile']['name'] != '') {
    $file -> upload_file();
}
if($_POST['delete']) {
    $file->delete_file();
}

if ($_POST['url']!='' && $_POST['checked'] == "true"){
    $file -> download_to_server();
}

$user_files = $dbXML-> get_user_files($id); // creating an array that contains info about user files
$tableTpl = new Template($tr, $app_parameters, $stat_data, $user_permits, $user_data, $user_files );

$tableTpl -> proccess_tpl_files();
echo $tableTpl -> get_file_contents();

}