        <div class="col col-1-5">
            <div class="information" id="information">
                <b>{DB="title_for_stat"}:</b><br>
                {DB="text_users_quantity"}: {SV="users_quantity"} <br>
                {DB="text_files_quantity"}: {SV="files_quantity"}<br>
                {DB="text_volume"}: {SV="total_volume"} Mb<br>
                {DB="text_ratio"}: {SV="ratio"} Mb<br>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
        <script src="js/sort.js"></script>
        <script src="js/xhr.js"></script>
<footer>
    <div class="">&copy; {DB="start_year"} {DV="footer_date"} {DB="text_footer"}</div>
</footer>

</body>
</html>