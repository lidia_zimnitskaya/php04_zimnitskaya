<tr class="tr-file">
    <td class="td-file">
        <a href="{LD="download_path"}{LD="hash_file_name"}"  download="{LD="file_name"}">{LD="file_name"}</a>
    </td>
    <td class="td-file">
        {LD="file_size_kb"}
    </td>
    <td class="td-file">
        {LD="table_date"}
    </td>
    <td class="td-file">
        <a href="?delete={LD="file_name"}" class="delete">{LD="action"}</a>
    </td>
</tr>