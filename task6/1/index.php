<?php
include_once('config.php');
include_once('class/DB.class.php');
include_once('class/Template.class.php');
include_once('class/User.class.php');
include_once('class/File.class.php');

$db = new DB($DB_con);
$user = new USER($DB_con);

// defining what template to handle
if ($user -> is_loggedin() != "") {
    $tpl = 'after_login.tpl';
    if($_SESSION['user_session']) {
        $id = $_SESSION['user_session'];
    }
    else {
        $id = $_COOKIE['Remember'];
    }
}
else {
    $tpl = 'before_login.tpl';
}

$app_parameters = $db -> return_application_parameters() ; // creating an array that contains text info about our application
$stat_data = $db -> get_stat_data(); // creating an array that contains statistic info about our application
$user_permits = $db-> get_user_permits($id); // creating an array that contains info about logged in user permissions
$user_data = $db-> get_user_info($id); // creating an array that contains user info
$user_files = $db-> get_user_files($id); // creating an array that contains info about user files

//creating the object with required parameters for template engine
$template = new Template($tpl, $app_parameters, $stat_data, $user_permits, $user_data, $user_files );

if($_GET['logout']) {
    $user->logout();
}

if ($_POST['submit'] && sizeof($_POST)!==0){
    $uname = $_POST['user'] ?? ''; //checking that the inputs not empty
    $upassword = $_POST['password'] ?? '';

    $uname = trim($uname);
    $upassword = trim($upassword);

    if (mb_strlen($uname)>0 && mb_strlen($upassword)>0){

        if($_POST['remember']!='on') {
            //logging the user in and redirecting to the page with upload forms and directory
            $user->login($uname, $upassword);
        }
        elseif ($_POST['remember']=='on') {

            $user->remember_me($uname, $upassword);
        }
    }
}

$file = new FILE($DB_con, $id, $user_permits, $user_data);

if($_POST['upload']) {
    $file -> upload_file();
}

if (isset($_POST['download']) && isset($_POST['url']) && $_POST['save_as'] == "on"){
    $file -> download_to_server();

}
elseif (isset($_POST['download']) && isset($_POST['url']) && $_POST['save_as'] == "") {
    $file -> download_to_pc();
}
if($_GET['delete']) {
    $file->delete_file();
}

$template -> proccess_tpl_files();
echo $template -> get_file_contents();
