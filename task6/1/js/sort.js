var originTable = document.getElementById('file-table'); // getting the table data
var tBody = document.getElementById('tbody');  //getting the array with only tbody rows


// checking if there are cookies with sort parameters
window.onload = function () {

    if(getCookie("col") != "" && getCookie("dataType") != "" && getCookie("sort") != "") {

        sort_table(tBody, getCookie("col"), getCookie("dataType"), getCookie("sort"));

    }
}


originTable.onclick = function(e) {

    var sortable = e.target.className; // identifying the thead cells that can be sorted
    var col = e.target.cellIndex; // identifying what cell was clicked
    var clickedCell = e.target.id;  //identifying the id of clicked cell
    var dataType = e.target.getAttribute('data-type'); // getting the data type to know how to sort, as a number or as a string
    var date = new Date(new Date().getTime() + 60 * 10000); // 10 minutes

    if (sortable == "th-file sortAsc") { //if the user click on the sortable column which should be sorted ascending
        document.getElementById(clickedCell).classList.remove('sortAsc'); // remove the class sortAsc that identify ascending sorting after click
        document.getElementById(clickedCell).classList.add('sortDesc'); // add class sortDesc , that means that after second click sorting will be descending
        sort_table(tBody, col, dataType, 1);

        // putting to cookies the sort parameters to remember them
        document.cookie = "col =" + col; +"expires=" + date.toUTCString();
        document.cookie = "dataType =" + dataType; +"expires=" + date.toUTCString();
        document.cookie = "sort = 1; expires=" + date.toUTCString();

    }
    else if (sortable == "th-file sortDesc") {
        document.getElementById(clickedCell).classList.remove('sortDesc'); // remove the class sortDesc that identify descending sorting after click
        document.getElementById(clickedCell).classList.add('sortAsc'); // add class sortAsc, that means that after second click sorting will be ascending
        sort_table(tBody, col, dataType, -1);

        // putting to cookies the sort parameters
        document.cookie = "col =" + col; +"expires=" + date.toUTCString();
        document.cookie = "dataType =" + dataType; +"expires=" + date.toUTCString();
        document.cookie = "sort = -1; expires=" + date.toUTCString();

    }
}


//sorting the table
function sort_table(tbody, col, dataType, sort) {
    var rows = tbody.rows,
        rowlen = rows.length,
        arr = new Array(),
        i, j, cells, celllen;
    // fill the array with cells from the table
    for (i = 0; i < rowlen; i++) {
        cells = rows[i].cells;
        celllen = cells.length;
        arr[i] = new Array();
        for (j = 0; j < celllen; j++) {
            arr[i][j] = cells[j].innerHTML;
        }
    }

    arr.sort(function (a, b) { // sorting the array by the specified column number (col) and order (sort) depending on data-type

        if (dataType == "number") {
            return (sort == 1) ? (a[col] - b[col]) : (b[col] - a[col]);

        }
        else  {
            return (a[col] == b[col]) ? 0 : ((a[col] > b[col]) ? sort : -1 * sort);
        }
    });
    // replacing existing rows with new rows created from the sorted array
    for (i = 0; i < rowlen; i++) {
        rows[i].innerHTML = "<td class='td-file'>" + arr[i].join("</td><td class='td-file'>") + "</td>";
    }
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

