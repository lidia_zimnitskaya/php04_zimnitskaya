<?php

declare(strict_types=1);

class DBdata
{
	private $message = array();
	private $label = array();

	public function __construct()
	{
		$mysql = DBconnect::getInstance();

		$r = $mysql->runQuery("SELECT * FROM `labels`");
		while ($row = $r->fetch(PDO::FETCH_ASSOC)) {
			$this->label[$row['l_name']] = $row['l_text'];
		}
		$r = $mysql->runQuery("SELECT * FROM `message`");
		while ($row = $r->fetch(PDO::FETCH_ASSOC)) {
			$this->message[$row['m_name']] = $row['m_text'];
		}
	}

    public function getAllLabels () : array
    {
        return $this->label;
    }
	
	public function getAllMessages () : array
    {
        return $this->message;
    }
}



