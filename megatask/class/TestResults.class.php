<?php

declare(strict_types=1);

class TestResults
{

    private $test_results = array();
    private $test_info = array();
    private $user = array();
    private $lastId ;

    public function setUserData(array $array)
    {
        return $this->user = $array;
    }

    public function insertAnswers($var)
    {
        //getting parameters for a query
        $questionId = $var[2];
        $answerId = $var[1];
        $answer_t_or_f=$this->test_info[$var[0]];

        //insert the answers of the specified user to a specified test
        $mysql = DBconnect::getInstance();
        $mysql->runQuery("INSERT INTO `user_stat_answers`(`user_stat_id`, `q_id`, `a_id`, `answer`) VALUES (:user_stat_id, :q_id, :a_id, :answer)",
                         array(':user_stat_id'=>$this->lastId, ':q_id'=>$questionId, ':a_id'=>$answerId, ':answer'=>$answer_t_or_f));
    }

    public function insertTestAnswers(array $answers)
    {
        $this->test_info = $answers;
        $userId = $this->user['u_id'];
        $testId = $this->test_info['testId'];

        //insert the info about user and a test
        $mysql = DBconnect::getInstance();
        $mysql->runQuery("INSERT INTO `user_stat`(`u_id`, `t_id`) VALUES (:u_id, :t_id)",
                            array(':u_id'=>$userId, ':t_id'=>$testId));

        //getting the id of the test stat info to which we will connect the answers
        $stmt = $mysql->runQuery("SELECT LAST_INSERT_ID()");
        $this->lastId = $stmt->fetchColumn();

        foreach ($answers as $key => $value) {
            if (preg_match("/answer_(\d+)_question_(\d+)/", $key)) {
                $array[]=$key;
                $array=preg_replace_callback("/answer_(\d+)_question_(\d+)/", array($this, 'insertAnswers'), $array);
            }
        }
        return $this->test_info;
    }

    public function showResults()
    {
        $mysql = DBconnect::getInstance();

        //get the array with users answers to the last test
        $stmt = $mysql->runQuery("SELECT `user_stat`.`t_id`, `test`.`t_name`, `user_stat_answers`.`q_id`, `question`.`q_text`, `user_stat_answers`.`a_id`, `answer`.`a_text`, `user_stat_answers`.`answer`
                                    FROM `user_stat` 
                                    INNER JOIN `user_stat_answers` ON `user_stat`.`user_stat_id`=`user_stat_answers`.`user_stat_id` 
                                    INNER JOIN `answer` ON `answer`.`a_id`=`user_stat_answers`.`a_id`
                                    INNER JOIN `question` ON `question`.`q_id`=`user_stat_answers`.`q_id`
                                    INNER JOIN `test` ON `test`.`t_id`=`user_stat`.`t_id`
                                    WHERE `user_stat_answers`.`user_stat_id`=:user_stat_id",
                                    array(':user_stat_id'=>$this->lastId));
        $this->test_results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($this->test_results as $item) {
            $array['t_id']=$item['t_id'];
            $array['t_name']=$item['t_name'];
            $array['questions_id'][$item['q_id']][$item['a_id']]=$item['answer'];
            $array['questions_answer'][$item['q_text']][$item['a_id']]=$item['answer'];
            $array['questions'][$item['q_text']][$item['a_id']]=$item['a_text'];
        }

        //get the correct answers array for the same test
        $correct = $mysql->runQuery("SELECT `user_stat`.`t_id`, `test`.`t_name`, `user_stat_answers`.`q_id`, `question`.`q_text`, `user_stat_answers`.`a_id`, `answer`.`a_text`, `question_answer`.`qa_right_or_not` 
                                    FROM `user_stat` 
                                    INNER JOIN `user_stat_answers` ON `user_stat`.`user_stat_id`=`user_stat_answers`.`user_stat_id` 
                                    INNER JOIN `answer` ON `answer`.`a_id`=`user_stat_answers`.`a_id` 
                                    INNER JOIN `question` ON `question`.`q_id`=`user_stat_answers`.`q_id` 
                                    INNER JOIN `test` ON `test`.`t_id`=`user_stat`.`t_id` 
                                    INNER JOIN `question_answer` ON `question_answer`.`qa_question_id`=`user_stat_answers`.`q_id` AND `question_answer`.`qa_answer_id`=`user_stat_answers`.`a_id`
                                    WHERE `user_stat_answers`.`user_stat_id`=:user_stat_id",
            array(':user_stat_id'=>$this->lastId));
        $answers = $correct->fetchAll(PDO::FETCH_ASSOC);
        foreach ($answers as $item) {
            $correct_array['t_id']=$item['t_id'];
            $correct_array['t_name']=$item['t_name'];
            $correct_array['questions_id'][$item['q_id']][$item['a_id']]=$item['qa_right_or_not'];
            $correct_array['questions_answer'][$item['q_text']][$item['a_id']]=$item['qa_right_or_not'];
            $correct_array['questions'][$item['q_text']][$item['a_id']]=$item['a_text'];
        }

        // now we are comparing user's answers and correct ones and echo the result
        // (It's a temporary solution, until i will update a better one)
        $text='<div style="margin: 15px">Your results of the test <b>'.$array['t_name'].'</b><br />';
        foreach ($array['questions_answer'] as $key=>$value){
            $correct_answers = array_intersect_assoc($value, $correct_array['questions_answer'][$key]);
            $wrong_answers = array_diff_assoc($value, $correct_array['questions_answer'][$key]);
            $text.= 'Question - ' .$key.'<br /><ul>';
            foreach ($value as $k=>$v){
                if(isset($correct_answers[$k])){
                    if($correct_answers[$k]=="1"){
                        $text.='<li style="color: green; font-weight: bold">'.$array['questions'][$key][$k].' - correct answer</li>';
                    }
                    elseif($correct_answers[$k]=="0"){
                        $text.='<li>'.$array['questions'][$key][$k].'</li>';
                    }
                }
                elseif (isset($wrong_answers[$k])){
                    if($wrong_answers[$k]=="1"){
                        $text.='<li style="color: red; font-weight: bold">'.$array['questions'][$key][$k].' - wrong answer</li>';
                    }
                    elseif($wrong_answers[$k]=="0"){
                        $text.='<li style="color: crimson;">'.$array['questions'][$key][$k].' - you should choose</li>';
                    }
                }

            }
            $text.='</ul>';

        }
        $text.='</div>';
        echo $text;
    }
}