<?php

declare(strict_types=1);

class Test
{
    private $test_categories = array();
    private $test_questions = array();
    private $test_names = array();
    private $test_info = array();
    private $question_info = array();
    private $answer_info = array();

    public function __construct()
    {
        $mysql = DBconnect::getInstance();

        $r = $mysql->runQuery("SELECT * FROM `test_categories` ORDER BY `category_id` ASC");
        $this->test_categories = $r->fetchAll(PDO::FETCH_ASSOC);

        $r = $mysql->runQuery("SELECT * FROM `question`");
        $this->array = $r->fetchAll(PDO::FETCH_ASSOC);

        $r = $mysql->runQuery("SELECT `test`.`t_id`, `test`.`t_name`, `test`.`time`, `test`.`category_id`, `test_categories`.`category_name` FROM `test` INNER JOIN `test_categories` ON   `test`.`category_id`=`test_categories`.`category_id`");
        $this->test_names = $r->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getTestCategories () : array
    {
        return $this->test_categories;
    }

    public function getTestQuestions () : array
    {
        $i=1;
        foreach ($this->array as $value) {
            $value['counter'] = $i++;
            $this->test_questions[]=$value;
        }
        return $this->test_questions;
    }

    public function getTestNames () : array
    {
        return $this->test_names;
    }

    public function insertTest(array $test_data) :void
    {
        $mysql = DBconnect::getInstance();
        $mysql->runQuery("INSERT INTO `test`(`t_name`, `category_id`, `time`) VALUES (:t_name, :category_id, :time)",
                        array(':t_name'=>$_POST['testName'], ':category_id'=>$_POST['testCategoryId'], ':time'=>$_POST['timeLimit']));

        //getting the id of the test
        $stmt = $mysql->runQuery("SELECT LAST_INSERT_ID()");
        $lastId = $stmt->fetchColumn();

        //inserting the test questions
        for($i=1; $i<=$test_data['quantity']; $i++) {
            if(isset($test_data['questionId'.$i])) {
                $mysql->runQuery("INSERT INTO `test_questions`(`tq_question_id`, `tq_test_id`) VALUES (:tq_question_id, :tq_test_id)",
                                array(':tq_question_id'=>$_POST['questionId'.$i], ':tq_test_id'=>$lastId));
            }
        }
    }

    public function getTestInfo(string $test_id) : array
    {
        $mysql = DBconnect::getInstance();
        $stmt = $mysql->runQuery("SELECT `test`.`t_id`, `test`.`t_name`, `test`.`time`, `test_categories`.`category_name` 
                                    FROM `test` 
                                    INNER JOIN `test_categories` ON `test`.`category_id`=`test_categories`.`category_id` 
                                    WHERE `t_id`=:testId",
                                    array(':testId'=>$test_id));
        $this->test_info = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->test_info;
    }

    public function showTest(array $test_data) //: ?array
    {
        $mysql = DBconnect::getInstance();
        $stmt = $mysql->runQuery("SELECT `test`.`t_name`, `test`.`time`, `question`.`q_id`, `question`.`q_text`, `question`.`q_type`, `question`.`q_time`, `question`.`q_picture` 
                            FROM `test_questions` 
                            INNER JOIN `question` ON `test_questions`.`tq_question_id`=`question`.`q_id` 
                            INNER JOIN `test` ON `test_questions`.`tq_test_id`=`test`.`t_id` 
                            WHERE `tq_test_id`=:testId",
                            array(':testId'=>$test_data['test']));
        $testTable= $stmt->fetchAll(PDO::FETCH_ASSOC);
        $i=1;
        foreach ($testTable as $value){
            $value['num'] = $i;
            $i++;
            $this->question_info[] = $value;

        }
        return $this->question_info;
    }

    public function getAnswersInfo()
    {
        $mysql = DBconnect::getInstance();
        $stmt = $mysql->runQuery("SELECT `question_answer`.`qa_question_id`,`answer`.`a_id`, `answer`.`a_text`, `question_answer`.`qa_right_or_not`, `question`.`q_type` 
                                    FROM `question_answer` 
                                    INNER JOIN `answer` ON `question_answer`.`qa_answer_id`=`answer`.`a_id` 
                                    INNER JOIN `question` ON `question`.`q_id`=`question_answer`.`qa_question_id` 
                                    ORDER BY `question_answer`.`qa_question_id` ASC");
        $array = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($array as $value){

            if($value['q_type']==='1'){
                $value['input_type']='type="radio"';
            }
            elseif ($value['q_type']==='2'){
                $value['input_type']='type="checkbox"';
            }
            $this->answer_info[] = $value;
        }

        return $this->answer_info;
    }
}