<?php

declare(strict_types=1);

session_start();

require_once('config.php');

// Autoload classes.
function __autoload(string $classname) : void
{
    require_once('class/' . $classname . '.class.php');
}

//Create objects
$DB= new DBdata();
$UserRight = new UserRights();
$newTpl = new Template();
$question = new Question();
$test = new Test();
$TestAnswers = new TestResults();
$user_new = new User();

//define what template to use
if(isset($_POST['tpl'])) {
    $tpl = $_POST['tpl'];
}

//choose the scenario depending on the parameter sent by ajax
if(isset($_POST['queryType']) && $_POST['queryType']== 'changeClass') {
    $UserRight -> changeUserClass($_POST['u_login'], $_POST['newClass']);
}
elseif (isset($_POST['queryType']) && $_POST['queryType']== 'insertQuestion'){
    $question -> insertQuestion($_POST, $_FILES);
}
elseif (isset($_POST['queryType']) && $_POST['queryType']== 'insertTest'){
    $test->insertTest($_POST);
}
elseif (isset($_POST['queryType']) && $_POST['queryType']== 'getTemplete') {
    $newTpl->setLabels($DB->getAllLabels());
    $newTpl->setUserRights($UserRight->getAllUsers());
    $newTpl->setUserClasses($UserRight->getAllUserClasses());
    $newTpl->setTestCategories($test->getTestCategories());
    $newTpl->setTestQuestions($test->getTestQuestions());
    $newTpl->readMainTemplate($tpl);
    $newTpl->proccess_tpl_files();
    echo $newTpl->get_file_contents();
}
elseif(isset($_POST['queryType']) && $_POST['queryType']== 'showTest') {

    $newTpl->setLabels($DB->getAllLabels());
    $newTpl->setTestInfo($test->getTestInfo($_POST['test']));
    $newTpl->setQuestionInfo($test->showTest($_POST));
    $newTpl->setAnswerData($test->getAnswersInfo());
    $newTpl->readMainTemplate($tpl);
    $newTpl->proccess_tpl_files();
    echo $newTpl->get_file_contents();
}
elseif(isset($_POST['queryType']) && $_POST['queryType']== 'answerTest') {

    $TestAnswers->setUserData($user_new -> getUserData());
    $TestAnswers->insertTestAnswers($_POST);
    $TestAnswers->showResults();
}