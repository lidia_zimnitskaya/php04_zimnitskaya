<div class="section">
    <div class="row">
        <div class="auth-string">
            {LABEL="greeting"} {UDATA="u_name"}. {LABEL="logged_message"}. <a href="?logout=true">{LABEL="text_logout"}</a>
        </div>
    </div>
</div>