<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="styles/style.css" rel="stylesheet">
    <title>{LABEL="title"}</title>
    <script src="js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div class="section">
    <div class="row">
        <h1>{LABEL="h1"}</h1>
    </div>
</div>
