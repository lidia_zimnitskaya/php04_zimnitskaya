<div>
    <div id="result_message" style="margin: 15px; color: darkblue;">{LABEL="new_test_title"}</div>
    <form>
        <table>
            <tr>
                <td>
                    Test's category:
                </td>
                <td colspan="2">
                    <select class="selects" id="test_category" name="test_category" >
                        {LOOP=categories="create_test_category.tpl"}
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Test's name:
                </td>
                <td colspan="2">
                    <input type="text" name="test_name_input" id="test_name_input" value="" required/>
                </td>
            </tr>
            <tr>
                <td>
                    Time limit (minutes):
                </td>
                <td colspan="2">
                    <input type="text" name="test_time_input" id="time_input" value="" required/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2" style="text-align: left; padding-top: 20px;">
                    <b>Choose questions:</b>
                </td>
            </tr>
                {LOOP=test_questions="create_test_questions.tpl"}
            <tr>
                <td>
                </td>
                <td colspan="2">
                    <input onclick="insertTest();" value="Create a test" class="button2"/>
                </td>
            </tr>
        </table>

    </form>
</div>
