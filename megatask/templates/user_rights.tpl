<div>
    <div id="result_message" style="margin: 15px; color: darkblue;">{LABEL="new_ur_title"}</div>
    <table id="user_rights_table">
        <thead>
        <tr>
            <th>
                {LABEL="table_text_login"}
            </th>
            <th>
                {LABEL="table_text_name"}
            </th>
            <th>
                {LABEL="table_text_type"}
            </th>
            <th>
                {LABEL="table_text_action"}
            </th>
        </tr>
        </thead>
        <tbody>
            {LOOP=user_rights="user_rights_tr.tpl"}
        </tbody>
    </table>
    <div style="clear:both;"></div>
</div>