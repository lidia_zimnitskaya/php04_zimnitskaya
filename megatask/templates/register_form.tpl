
<div class="section main_section">
    <div class="row work_area">
        <div class="col col-1-5">

        </div>
        <div class="col col-3-5 vertical_border">


            <form action="" method="post" enctype="">
            <div class="table">
			    <div class="tr">
                    <div class="td_text">                        
                    </div>
                    <div class="td" style="color:red;font-weight:bold;">
                      {DV="error_massage"}
                    </div>
                </div>
				<div class="tr">
					<div class="td_text">
                        {LABEL="text_name"}:
                    </div>
                    <div class="td">
                        <input type="text" class="input" required name="name">
                    </div>
                </div>
				<div class="tr">
					<div class="td_text">
                        {LABEL="text_email"}:
                    </div>
                    <div class="td">
                        <input type="text" class="input" required name="email">
                    </div>
                </div>
                <div class="tr">
                    <div class="td_text">
                        {LABEL="text_login"}:
                    </div>
                    <div class="td">
                        <input type="text" class="input" required name="user" value="">
                    </div>
                </div>
                <div class="tr">
                    <div class="td_text">
                        {LABEL="text_password"}:
                    </div>
                    <div class="td">
                        <input type="password" class="input" required name="password" value="">
                    </div>
                </div>
                <div class="tr">
                    <div class="td_text">

                    </div>
                    <div class="td">
                        <input type="submit" class="button" name="register" value="{LABEL="button_register"}" class="" />
                    </div>
                </div>
				<div class="tr">
                    <div class="td_text">
                        
                    </div>
                    <div class="td" style="font-size: 80%; color: black;">
						
                        {LABEL="text_already_registered"} <a href="?registered=true">{LABEL="button_already_registered"}</a>    
                    </div>
                </div>
            </div>
			
        </form>
		
            <div style="clear:both;"></div>

        </div>
        <div class="col col-1-5">
            <div class="information">
                
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>