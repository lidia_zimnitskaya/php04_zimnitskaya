<div>
    <div id="result_message" style="margin: 15px; color: darkblue;">{LABEL="new_question_title"}</div>
    <form>
        <table>
            <tr>
                <td>
                    Question text:
                </td>
                <td>
                    <input type="text" name="question_input" id="question_input" value="" required/>
                </td>
            </tr>
            <tr>
                <td>
                    Picture:
                </td>
                <td>
                    <input type="file" name="file" id="file" accept="image/jpeg,image/png,image/gif" data-max-size="250000" />
                </td>
            </tr>
            <tr>
                <td>
                    Answer 1:
                </td>
                <td>
                    <input type="text" name="answer1" class="answer1" required/> <span style="font-size: 60%">Right answer</span> <input type="checkbox" class="answer1_check" />
                </td>
            </tr>
            <tr>
                <td>
                    Answer 2:
                </td>
                <td>
                    <input type="text" name="answer2" class="answer2" required/> <span style="font-size: 60%">Right answer</span> <input type="checkbox" class="answer2_check" />
                </td>
            </tr>
            <tr>
                <td>
                    Answer 3:
                </td>
                <td>
                    <input type="text" name="answer3" class="answer3" /> <span style="font-size: 60%">Right answer</span> <input type="checkbox" class="answer3_check" />
                </td>
            </tr>
            <tr>
                <td>
                    Answer 4:
                </td>
                <td>
                    <input type="text" name="answer4" class="answer4" /> <span style="font-size: 60%">Right answer</span> <input type="checkbox" class="answer4_check" />
                </td>
            </tr>
            <tr>
                <td>
                    Answer 5:
                </td>
                <td>
                    <input type="text" name="answer5" class="answer5" /> <span style="font-size: 60%">Right answer</span> <input type="checkbox" class="answer5_check" />
                </td>
            </tr>
            <tr>
                <td>

                </td>
                <td>
                    <input onclick="inputQuestion();" value="Submit" class="button2"/>
                </td>
            </tr>
        </table>

    </form>
</div>