var handler = "server.php";

function showTest(e) {

    event.preventDefault(); // prevent browser default actions
    //getting parameters
    var test=$(e)
    var testId = e.id;
    testId= testId.replace( 'test_','');

    event.preventDefault(); // prevent browser default actions
    var request = $.ajax({
        url: handler,
        type: "post",
        data:  ({queryType : "showTest", tpl : "show_test.tpl", test : testId}) // sending the array with parameters
    });
    //  if success
    request.done(function (response, textStatus, jqXHR){

        $("#main_body").eq(0).html(response); // return the responce

    });
    // if fail
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    })

}

function sendTestAnswers(e) {

    var testId=$(e).attr("data-test-id");
    var answers = $(".test_answer");
    var answersQuantity = $(".test_answer").length;

    // collecting parameters for post array
    var fileData = new FormData();
    fileData.append("queryType", "answerTest");
    fileData.append("testId", testId);

    for(var i=0; i<answersQuantity; i++) {
        var answerId = $(".test_answer").eq(i).val();
        var checked = $("#"+answerId).prop("checked");
        if(checked===true){
            checked=1;
        }
        else if (checked===false){
            checked=0
        }
        var questionId = $("#"+answerId).prop("name");
        fileData.append(answerId, checked);
    }

    var request = $.ajax({
        url: handler, // point to server-side PHP script
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: fileData,
        type: 'post', // sending the array with parameters;
    });
    //  if success
    request.done(function (response, textStatus, jqXHR){

        $("#main_body").eq(0).html(response); // return the responce

    });
    // if fail
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });
}