var handler = "server.php";

function getUserRightsTable() {
    $("#result_message").eq(0).html("");
    event.preventDefault(); // prevent browser default actions
    var request = $.ajax({
        url: handler,
        type: "post",
        data:  ({queryType : "getTemplete", tpl : "user_rights.tpl"}) // sending the array with parameters
    });
    //  if success
    request.done(function (response, textStatus, jqXHR){

        $("#admin_body").eq(0).html(response); // return the responce

        // now we need to select the current user type in the select
        var select_arr = $("#user_rights_table select");
        for(var i = 0, len = select_arr.length; i < len; i++){
            var u_class = $(select_arr[i]).eq(0).attr('name');
            var select_kids = $(select_arr[i]).eq(0).children();
            var options = $(select_kids);

            for(var j = 0, len2 = options.length; j < len; j++) {
                if($(options[j]).val() == u_class) {
                    $(options[j]).attr("selected", "selected");
                    $(options[j]).css("background", "#ffffcc");
                }
            }

        }
    });
    // if fail
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });
};

/// underlight the changed rows
function changeFunc(e) {
    var td = $(e).parent();
    var tr = $(td).parent()[0];
    $(tr).css("background", "#ffffcc");

    //this function will be called only if the changes will be made
    $(document).on('click', '.urights_button', function(event) {

            var td = $(event.target).parent();
            var tr = $(td).parent()[0];
            var kids = $(tr).children()[2];
            var newClass = $(kids).children().eq(0).val();
            var u_login = event.target.name;

        event.preventDefault(); // prevent browser default actions
        var request = $.ajax({
            url: handler,
            type: "post",
            data:  ({queryType : "changeClass", newClass : newClass, u_login : u_login}) // sending the array with parameters
        });
        //  if success
        request.done(function (response, textStatus, jqXHR){
            getUserRightsTable();
            $("#result_message").eq(0).css("color", "darkgreen");
            $("#result_message").eq(0).html("A new user class <b>"+newClass+"</b> for <b>" +u_login+"</b> was settled!");
        });
        // if fail
        request.fail(function (jqXHR, textStatus, errorThrown){
            // Log the error to the console
            console.error(
                "The following error occurred: "+
                textStatus, errorThrown
            );
        });
    });
}

function createQuestion() {
    $("#result_message").eq(0).html("");

    event.preventDefault(); // prevent browser default actions
    var request = $.ajax({
        url: handler,
        type: "post",
        data:  ({queryType : "getTemplete", tpl : "create_question.tpl"}) // sending the array with parameters
    });
    //  if success
    request.done(function (response, textStatus, jqXHR){

        $("#admin_body").eq(0).html(response); // return the responce

    });
    // if fail
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });
}

function inputQuestion() {

    event.preventDefault(); // prevent browser default actions

    //getting parameters
    var question=$("#question_input").val();
    var file = $("#file").prop('files')[0];
    var answer1 = $(".answer1").val();
    var checked1 = $(".answer1_check").prop("checked");
    var answer2 = $(".answer2").val();
    var checked2 = $(".answer2_check").prop("checked");
    var answer3 = $(".answer3").val();
    var checked3 = $(".answer3_check").prop("checked");
    var answer4 = $(".answer4").val();
    var checked4 = $(".answer4_check").prop("checked");
    var answer5 = $(".answer5").val();
    var checked5 = $(".answer5_check").prop("checked");
    var maxSize = $("#file").attr('data-max-size');

    //checking the image size
    if(file!==undefined){
        if(file.size>maxSize){
            $("#result_message").eq(0).css("color", "red");
            $("#result_message").eq(0).html("Too large image! Max size is "+maxSize/1000+" kb");
            return false;
        }
    }
    if(question=='') {
        $("#result_message").eq(0).css("color", "red");
        $("#result_message").eq(0).html("Enter a question.");
        return false;
    }
    else if (answer1=='' || answer2=='') {
        $("#result_message").eq(0).css("color", "red");
        $("#result_message").eq(0).html("Enter at least two possible answers.");
        return false;
    }
    else if (checked1 === false && checked2 === false && checked3 === false && checked4 === false && checked5 === false) {
        $("#result_message").eq(0).css("color", "red");
        $("#result_message").eq(0).html("Choose at least one correct answer.");
        return false;
    }

    // collecting parameters for post array
    var fileData = new FormData();
    fileData.append("queryType", "insertQuestion");
    fileData.append("question_text", question);
    fileData.append("file", file);
    fileData.append("answer1", answer1);
    fileData.append("answer1_right", checked1);
    fileData.append("answer2", answer2);
    fileData.append("answer2_right", checked2);
    fileData.append("answer3", answer3);
    fileData.append("answer3_right", checked3);
    fileData.append("answer4", answer4);
    fileData.append("answer4_right", checked4);
    fileData.append("answer5", answer5);
    fileData.append("answer5_right", checked5);


    var request = $.ajax({
        url: handler, // point to server-side PHP script
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: fileData,
        type: 'post', // sending the array with parameters;
    });
    //  if success
    request.done(function (response, textStatus, jqXHR){

        createQuestion();
        $("#result_message").eq(0).html("A question was added successfully.");

    });
    // if fail
    request.fail(function (jqXHR, textStatus, errorThrown){
        $("#result_message").eq(0).css("color", "red");
        $("#result_message").eq(0).html("Error.Something went wrong, please, try again.");
    });

}


$(window).on('load', function(event) {
    //var question=$("#question_input").val();
    //console.log($("#question_input")[0]);
});


function createTest() {
    $("#result_message").eq(0).html("");
    event.preventDefault(); // prevent browser default actions
    var request = $.ajax({
        url: handler,
        type: "post",
        data:  ({queryType : "getTemplete", tpl : "create_test.tpl"}) // sending the array with parameters
    });
    //  if success
    request.done(function (response, textStatus, jqXHR){

        $("#admin_body").eq(0).html(response); // return the responce

    });
    // if fail
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });
}

function insertTest() {

    event.preventDefault(); // prevent browser default actions
    // get parameters
    var testCategoryId = $('#test_category').val();
    var testName = $('#test_name_input').val();
    var timeLimit = $('#time_input').val();
    var quantity = $('.questions_counter').length;

    //the test name must be filled
    if(testName==""){
        $("#result_message").eq(0).css("color", "red");
        $("#result_message").eq(0).html("Enter a name of a test.");
        return false;
    }

    // collecting parameters for post array
    var fileData = new FormData();
    fileData.append("queryType", "insertTest");
    fileData.append("testCategoryId", testCategoryId);
    fileData.append("testName", testName);
    fileData.append("timeLimit", timeLimit);
    fileData.append("quantity", quantity);

    var cheker=0; // will count how many questions were cheched
    for(var i=1; i<=quantity; i++) {
        if($("#question_"+i).prop("checked")===true){
            fileData.append("questionId"+i, $("#question_"+i).val());
            cheker++;
        }
    }
    if (cheker===0){
        $("#result_message").eq(0).css("color", "red");
        $("#result_message").eq(0).html("Choose questions for a test.");
        return false;
    }

    var request = $.ajax({
        url: handler, // point to server-side PHP script
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: fileData,
        type: 'post', // sending the array with parameters;
    });
    //  if success
    request.done(function (response, textStatus, jqXHR){
        createTest();
        $("#result_message").eq(0).css("color", "darkgreen");
        $("#result_message").eq(0).html("A test was added successfully.");

    });
    // if fail
    request.fail(function (jqXHR, textStatus, errorThrown){
        $("#result_message").eq(0).css("color", "red");
        $("#result_message").eq(0).html("Error.Something went wrong, please, try again.");
    });
}