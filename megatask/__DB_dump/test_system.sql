-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 09, 2017 at 09:17 PM
-- Server version: 5.7.18-0ubuntu0.17.04.1
-- PHP Version: 7.1.6-1~ubuntu17.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `a_id` int(10) UNSIGNED NOT NULL COMMENT 'Первичный ключ',
  `a_text` varchar(50) NOT NULL COMMENT 'Текст ответа'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`a_id`, `a_text`) VALUES
(142, ''),
(124, 'Blue square'),
(125, 'Blue triangle'),
(132, 'Brest'),
(140, 'Gomel'),
(122, 'Green square'),
(138, 'Lida'),
(133, 'Minsk'),
(131, 'Moskow'),
(127, 'No'),
(135, 'Paris'),
(123, 'Red triangle'),
(139, 'Smolensk'),
(134, 'Toronto'),
(121, 'Yellow circle'),
(126, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `l_id` int(11) NOT NULL COMMENT 'Первичный ключ',
  `l_name` varchar(100) NOT NULL COMMENT 'Имя переменной в верстке',
  `l_text` varchar(200) NOT NULL COMMENT 'Значение для сайта'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`l_id`, `l_name`, `l_text`) VALUES
(1, 'title', 'Testing system'),
(2, 'h1', 'Testing system'),
(3, 'login_message', 'You need to log in or register before continuing.'),
(4, 'start_year', '2016'),
(5, 'text_footer', 'EPAM RD PHP Training'),
(6, 'text_login', 'Login'),
(7, 'text_password', 'Password'),
(8, 'text_remember', 'Remember me'),
(9, 'button_login', 'Log in'),
(10, 'text_not_registered', 'Not registered?'),
(11, 'button_not_registered', 'Register here.'),
(12, 'logged_message', 'Welcome to testing system'),
(13, 'text_logout', 'Log out'),
(14, 'text_name', 'Your name'),
(15, 'text_email', 'Your email'),
(16, 'button_register', 'Register'),
(17, 'text_already_registered', 'Already registered? '),
(18, 'button_already_registered', 'Log in.'),
(19, 'greeting', 'Hello,'),
(20, 'table_text_login', 'User login'),
(21, 'table_text_name', 'User name'),
(22, 'table_text_type', 'User current type'),
(23, 'table_text_action', 'Confirm'),
(24, 'admin_menu_userR', 'User rights management'),
(25, 'admin_menu_createQ', 'Create a question'),
(26, 'admin_menu_createT', 'Create a test'),
(27, 'new_test_title', 'Create a new test:'),
(28, 'new_question_title', 'Create a new question:'),
(29, 'new_ur_title', 'A list of users and their classes:');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `m_id` int(11) NOT NULL COMMENT 'Первичный ключ',
  `m_name` varchar(200) NOT NULL COMMENT 'Значение переменной',
  `m_text` varchar(300) NOT NULL COMMENT 'Текст ответа'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`m_id`, `m_name`, `m_text`) VALUES
(1, 'wrong_login_or_password', 'Wrong login or password'),
(2, 'login_already_exist', 'Login or email already exist, please fill a different login or email.');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `q_id` int(10) UNSIGNED NOT NULL,
  `q_text` varchar(500) DEFAULT NULL,
  `q_type` int(11) UNSIGNED DEFAULT NULL,
  `q_weight` int(11) DEFAULT NULL,
  `q_time` int(11) DEFAULT NULL,
  `q_picture` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`q_id`, `q_text`, `q_type`, `q_weight`, `q_time`, `q_picture`) VALUES
(27, 'What is on the picture?', 1, NULL, NULL, 'square.png'),
(28, 'Is a triangle a polygon with three edges and three vertices?', 1, NULL, NULL, 'white.jpg'),
(29, 'What is the capital of Belarus?', 1, NULL, NULL, 'white.jpg'),
(30, 'Choose all belarusian cities.', 2, NULL, NULL, 'white.jpg'),
(31, 'Is France a shengen country?', 1, NULL, NULL, 'white.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `question_answer`
--

CREATE TABLE `question_answer` (
  `qa_id` int(10) UNSIGNED NOT NULL,
  `qa_question_id` int(11) UNSIGNED DEFAULT NULL,
  `qa_answer_id` int(11) UNSIGNED DEFAULT NULL,
  `qa_right_or_not` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question_answer`
--

INSERT INTO `question_answer` (`qa_id`, `qa_question_id`, `qa_answer_id`, `qa_right_or_not`) VALUES
(111, 27, 121, 0),
(112, 27, 122, 0),
(113, 27, 123, 0),
(114, 27, 124, 1),
(115, 27, 125, 0),
(116, 28, 126, 1),
(117, 28, 127, 0),
(121, 29, 131, 0),
(122, 29, 132, 0),
(123, 29, 133, 1),
(124, 29, 134, 0),
(125, 29, 135, 0),
(126, 30, 131, 0),
(127, 30, 132, 1),
(128, 30, 138, 1),
(129, 30, 139, 0),
(130, 30, 140, 1),
(131, 31, 126, 1),
(132, 31, 127, 0),
(134, 31, 142, 0);

-- --------------------------------------------------------

--
-- Table structure for table `question_types`
--

CREATE TABLE `question_types` (
  `qt_id` int(10) UNSIGNED NOT NULL,
  `qt_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question_types`
--

INSERT INTO `question_types` (`qt_id`, `qt_name`) VALUES
(1, 'single answer'),
(2, 'multiple answers');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `t_id` int(10) UNSIGNED NOT NULL,
  `t_name` varchar(50) DEFAULT NULL,
  `category_id` int(200) UNSIGNED NOT NULL,
  `time` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`t_id`, `t_name`, `category_id`, `time`) VALUES
(2, 'Geography', 2, 20),
(4, 'Math', 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `test_categories`
--

CREATE TABLE `test_categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_categories`
--

INSERT INTO `test_categories` (`category_id`, `category_name`) VALUES
(3, 'Advanced'),
(2, 'Basic level'),
(1, 'Elementary level');

-- --------------------------------------------------------

--
-- Table structure for table `test_questions`
--

CREATE TABLE `test_questions` (
  `tq_id` int(10) UNSIGNED NOT NULL,
  `tq_question_id` int(11) UNSIGNED DEFAULT NULL,
  `tq_test_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_questions`
--

INSERT INTO `test_questions` (`tq_id`, `tq_question_id`, `tq_test_id`) VALUES
(7, 27, 4),
(8, 28, 4),
(4, 29, 2),
(5, 30, 2),
(6, 31, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_id` int(10) UNSIGNED NOT NULL COMMENT 'Первичный ключ',
  `u_login` varchar(50) NOT NULL COMMENT 'Логин пользователя',
  `u_password` varchar(80) NOT NULL COMMENT 'hash-пароля',
  `u_name` varchar(100) NOT NULL COMMENT 'Имя пользователя',
  `u_email` varchar(100) NOT NULL COMMENT 'Эл.почта пользователя',
  `u_type` int(11) UNSIGNED NOT NULL COMMENT 'Тип пользователя',
  `u_remember` varchar(100) DEFAULT NULL COMMENT 'hash-coockie для долговременной авторизации'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `u_login`, `u_password`, `u_name`, `u_email`, `u_type`, `u_remember`) VALUES
(2, 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Admin', 'admin@testsys.by', 1, ''),
(3, 'moooon', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Lida', 'moooon@tut.by', 3, NULL),
(4, 'kukusik', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Lidi', 'iamliad@mail.ru', 2, NULL),
(5, 'root', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Peter Smith', 'peter@mail.com', 2, 'db87e1457c27897d921e15c6861663f67e17fcb1');

-- --------------------------------------------------------

--
-- Table structure for table `user_classes`
--

CREATE TABLE `user_classes` (
  `cl_id` int(10) UNSIGNED NOT NULL COMMENT 'Первичный ключ',
  `cl_name` varchar(50) NOT NULL COMMENT 'Тип пользователя',
  `cl_view` int(11) NOT NULL COMMENT 'Разрешен ли просмотр сайта',
  `cl_edit` int(11) NOT NULL COMMENT 'Разрешено ли редактирование',
  `cl_remove` int(11) NOT NULL COMMENT 'Разрешено ли удалять содержимое',
  `cl_change_user_rights` int(11) NOT NULL COMMENT 'Разрешено ли менять тип пользователя'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_classes`
--

INSERT INTO `user_classes` (`cl_id`, `cl_name`, `cl_view`, `cl_edit`, `cl_remove`, `cl_change_user_rights`) VALUES
(1, 'admin', 1, 1, 1, 1),
(2, 'visitor', 1, 0, 0, 0),
(3, 'RD-manager', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_stat`
--

CREATE TABLE `user_stat` (
  `user_stat_id` int(10) UNSIGNED NOT NULL,
  `u_id` int(10) UNSIGNED NOT NULL,
  `t_id` int(10) UNSIGNED NOT NULL,
  `us_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_stat`
--

INSERT INTO `user_stat` (`user_stat_id`, `u_id`, `t_id`, `us_time`) VALUES
(18, 5, 4, NULL),
(19, 5, 2, NULL),
(20, 5, 2, NULL),
(21, 5, 2, NULL),
(22, 5, 4, NULL),
(23, 5, 2, NULL),
(24, 5, 4, NULL),
(25, 5, 4, NULL),
(26, 5, 4, NULL),
(27, 5, 2, NULL),
(28, 5, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_stat_answers`
--

CREATE TABLE `user_stat_answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_stat_id` int(10) UNSIGNED NOT NULL,
  `q_id` int(10) UNSIGNED NOT NULL,
  `a_id` int(10) UNSIGNED NOT NULL,
  `answer` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_stat_answers`
--

INSERT INTO `user_stat_answers` (`id`, `user_stat_id`, `q_id`, `a_id`, `answer`) VALUES
(30, 18, 27, 121, 1),
(31, 18, 27, 124, 0),
(32, 18, 27, 123, 0),
(33, 18, 27, 122, 0),
(34, 18, 27, 125, 0),
(35, 18, 28, 127, 0),
(36, 18, 28, 126, 1),
(37, 19, 29, 133, 1),
(38, 19, 29, 132, 0),
(39, 19, 29, 135, 0),
(40, 19, 29, 131, 0),
(41, 19, 29, 134, 0),
(42, 19, 30, 132, 0),
(43, 19, 30, 140, 1),
(44, 19, 30, 131, 1),
(45, 19, 30, 139, 0),
(46, 19, 30, 138, 1),
(47, 19, 31, 126, 1),
(48, 19, 31, 127, 0),
(49, 20, 29, 133, 0),
(50, 20, 29, 132, 1),
(51, 20, 29, 135, 0),
(52, 20, 29, 131, 0),
(53, 20, 29, 134, 0),
(54, 20, 30, 132, 1),
(55, 20, 30, 140, 0),
(56, 20, 30, 131, 1),
(57, 20, 30, 139, 0),
(58, 20, 30, 138, 1),
(59, 20, 31, 126, 0),
(60, 20, 31, 127, 1),
(61, 21, 29, 133, 0),
(62, 21, 29, 132, 0),
(63, 21, 29, 135, 0),
(64, 21, 29, 131, 0),
(65, 21, 29, 134, 0),
(66, 21, 30, 132, 0),
(67, 21, 30, 140, 0),
(68, 21, 30, 131, 0),
(69, 21, 30, 139, 0),
(70, 21, 30, 138, 0),
(71, 21, 31, 126, 0),
(72, 21, 31, 127, 0),
(73, 22, 27, 121, 0),
(74, 22, 27, 124, 0),
(75, 22, 27, 123, 0),
(76, 22, 27, 122, 0),
(77, 22, 27, 125, 0),
(78, 22, 28, 127, 0),
(79, 22, 28, 126, 0),
(80, 23, 29, 133, 1),
(81, 23, 29, 132, 0),
(82, 23, 29, 135, 0),
(83, 23, 29, 131, 0),
(84, 23, 29, 134, 0),
(85, 23, 30, 132, 1),
(86, 23, 30, 140, 1),
(87, 23, 30, 131, 0),
(88, 23, 30, 139, 0),
(89, 23, 30, 138, 1),
(90, 23, 31, 126, 1),
(91, 23, 31, 127, 0),
(92, 24, 27, 121, 1),
(93, 24, 27, 124, 0),
(94, 24, 27, 123, 0),
(95, 24, 27, 122, 0),
(96, 24, 27, 125, 0),
(97, 24, 28, 127, 0),
(98, 24, 28, 126, 1),
(99, 25, 27, 121, 0),
(100, 25, 27, 124, 1),
(101, 25, 27, 123, 0),
(102, 25, 27, 122, 0),
(103, 25, 27, 125, 0),
(104, 25, 28, 127, 1),
(105, 25, 28, 126, 0),
(106, 26, 27, 121, 0),
(107, 26, 27, 124, 1),
(108, 26, 27, 123, 0),
(109, 26, 27, 122, 0),
(110, 26, 27, 125, 0),
(111, 26, 28, 127, 0),
(112, 26, 28, 126, 1),
(113, 27, 29, 133, 1),
(114, 27, 29, 132, 0),
(115, 27, 29, 135, 0),
(116, 27, 29, 131, 0),
(117, 27, 29, 134, 0),
(118, 27, 30, 132, 0),
(119, 27, 30, 140, 1),
(120, 27, 30, 131, 1),
(121, 27, 30, 139, 1),
(122, 27, 30, 138, 0),
(123, 27, 31, 126, 1),
(124, 27, 31, 127, 0),
(125, 28, 27, 121, 0),
(126, 28, 27, 124, 0),
(127, 28, 27, 123, 0),
(128, 28, 27, 122, 0),
(129, 28, 27, 125, 0),
(130, 28, 28, 127, 0),
(131, 28, 28, 126, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`a_id`),
  ADD UNIQUE KEY `UNQ_answer` (`a_text`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`l_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`q_id`),
  ADD UNIQUE KEY `UNQ_question` (`q_text`,`q_type`,`q_picture`),
  ADD KEY `IXFK_question_question_types` (`q_type`);

--
-- Indexes for table `question_answer`
--
ALTER TABLE `question_answer`
  ADD PRIMARY KEY (`qa_id`),
  ADD UNIQUE KEY `UNQ_qa` (`qa_question_id`,`qa_answer_id`),
  ADD KEY `IXFK_question_answer_answer` (`qa_answer_id`),
  ADD KEY `IXFK_question_answer_question` (`qa_question_id`);

--
-- Indexes for table `question_types`
--
ALTER TABLE `question_types`
  ADD PRIMARY KEY (`qt_id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`t_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `test_categories`
--
ALTER TABLE `test_categories`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `category_name` (`category_name`);

--
-- Indexes for table `test_questions`
--
ALTER TABLE `test_questions`
  ADD PRIMARY KEY (`tq_id`),
  ADD UNIQUE KEY `UNQ_test_questions` (`tq_question_id`,`tq_test_id`),
  ADD KEY `IXFK_test_questions_question` (`tq_question_id`),
  ADD KEY `IXFK_test_questions_test` (`tq_test_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`),
  ADD UNIQUE KEY `UNQ_user_login` (`u_login`),
  ADD UNIQUE KEY `UNQ_user_email` (`u_email`),
  ADD KEY `IXFK_user_user_classes` (`u_type`);

--
-- Indexes for table `user_classes`
--
ALTER TABLE `user_classes`
  ADD PRIMARY KEY (`cl_id`),
  ADD UNIQUE KEY `UNQ_class` (`cl_name`),
  ADD KEY `IDX_class` (`cl_id`);

--
-- Indexes for table `user_stat`
--
ALTER TABLE `user_stat`
  ADD PRIMARY KEY (`user_stat_id`),
  ADD KEY `u_id` (`u_id`);

--
-- Indexes for table `user_stat_answers`
--
ALTER TABLE `user_stat_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_stat_id` (`user_stat_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `a_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ', AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `l_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ', AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `q_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `question_answer`
--
ALTER TABLE `question_answer`
  MODIFY `qa_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT for table `question_types`
--
ALTER TABLE `question_types`
  MODIFY `qt_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `t_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `test_categories`
--
ALTER TABLE `test_categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `test_questions`
--
ALTER TABLE `test_questions`
  MODIFY `tq_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ', AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_classes`
--
ALTER TABLE `user_classes`
  MODIFY `cl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_stat`
--
ALTER TABLE `user_stat`
  MODIFY `user_stat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `user_stat_answers`
--
ALTER TABLE `user_stat_answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`q_type`) REFERENCES `question_types` (`qt_id`);

--
-- Constraints for table `question_answer`
--
ALTER TABLE `question_answer`
  ADD CONSTRAINT `question_answer_ibfk_1` FOREIGN KEY (`qa_answer_id`) REFERENCES `answer` (`a_id`),
  ADD CONSTRAINT `question_answer_ibfk_2` FOREIGN KEY (`qa_question_id`) REFERENCES `question` (`q_id`);

--
-- Constraints for table `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `test_categories` (`category_id`);

--
-- Constraints for table `test_questions`
--
ALTER TABLE `test_questions`
  ADD CONSTRAINT `test_questions_ibfk_1` FOREIGN KEY (`tq_test_id`) REFERENCES `test` (`t_id`),
  ADD CONSTRAINT `test_questions_ibfk_2` FOREIGN KEY (`tq_question_id`) REFERENCES `question` (`q_id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`u_type`) REFERENCES `user_classes` (`cl_id`);

--
-- Constraints for table `user_stat`
--
ALTER TABLE `user_stat`
  ADD CONSTRAINT `user_stat_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`);

--
-- Constraints for table `user_stat_answers`
--
ALTER TABLE `user_stat_answers`
  ADD CONSTRAINT `user_stat_answers_ibfk_1` FOREIGN KEY (`user_stat_id`) REFERENCES `user_stat` (`user_stat_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
