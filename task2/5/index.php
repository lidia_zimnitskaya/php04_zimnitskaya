<?php
//the log in form
function get_login_form($user = '', $password = ''){
    $form  = '';
    $form .= '<form action="" method="post" enctype="" style="margin-left: 50px; margin-top: 50px;">';
    $form .= '<input type="text" id="user" name="user" placeholder="User" value="' . $user . '" style="margin-right: 5px;">';
    $form .= '<input type="password" name="password" placeholder="Password" value="' . $password . '" style="margin-right: 10px;">';
    $form .= '<input type="submit" name="submit" value="Login" /></form>';
    return $form;

}

//the check of login data
function login($user, $password){
    include('config.php');
    if ($users[$user] == $password){
        setcookie("LOGIN", 'remember' , time()+3600);
        setcookie("USER", $user , time()+3600);
        echo download_form();
    }
    else {
        echo "Username and/or password incorrect. Please try again.<br>".get_login_form();
    }
}

//the form of uploading a file
function download_form(){

    $form = '';
    $form .= '<form enctype="" action="" method="POST">';
    $form .= '<input name="url" type="text" placeholder="URL" style="width: 355px; margin-right: 10px;"/>';
    $form .= '<input type="submit" name="download" value="Download" /></form>';
    return $form;
}


//checking what data to show
if($_COOKIE['LOGIN']== 'remember') {

    echo download_form();
}
else {

    if ($_POST['submit'] && sizeof($_POST)!==0){

        $user = (isset($_POST['user'])) ? $_POST['user'] : '-';
        $password = (isset($_POST['password'])) ? $_POST['password'] : '-';

        if (mb_strlen($user)>0 && mb_strlen($password)>0){

            login($user, $password);
        }
        else {
            echo "Please fill the form!<br>".get_login_form();

        }
    }
    else {
        echo get_login_form();
    }
}

if (isset($_POST['download']) && isset($_POST['url'])){
    set_time_limit (24 * 60 * 60);

    if (!isset($_POST['download'])) die();

    // server folder to save downloaded files to
    $destination_folder = '/var/www/task2/5/downloads/';

    $url = $_POST['url'];
    $newfname = $destination_folder . basename($url);

    $file = fopen ($url, "rb");
    if ($file) {
        $newf = fopen ($newfname, "wb");

        if ($newf)
            while(!feof($file)) {
                fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
            }
    }

   //using script to download files from server because some date have been already output, so header can't be used
   echo '<a href="/task2/5/downloads/'.basename($url).'" id="download_server" download></a>
        <script>
         document.getElementById("download_server").click();
        </script>
        Downloaded!';
    //header("Content-disposition: attachment;filename=".$newfname);
    //readfile($newfname);

}
