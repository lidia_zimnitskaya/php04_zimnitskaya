<?php

$users = array(
    "user"  => "987654",
    "admin" => "123456"
);

$ext=array(
    "jpg" => 12,
    "pdf" => 5,
    "zip" => 15
);

define("JPG", 2);
define("PDF", 5);
define("ZIP", 6);
define("TOTAL_VOLUME", 25);
