<?php
//the log in form
function get_login_form($user = '', $password = ''){
    $form  = '';
    $form .= '<form action="" method="post" enctype="" style="margin-left: 50px; margin-top: 50px;">';
    $form .= '<input type="text" id="user" name="user" placeholder="User" value="' . $user . '" style="margin-right: 5px;">';
    $form .= '<input type="password" name="password" placeholder="Password" value="' . $password . '" style="margin-right: 10px;">';
    $form .= '<input type="submit" name="submit" value="Login" /></form>';
    return $form;

}

//the check of login data
function login($user, $password){
    include('config.php');
    if ($users[$user] == $password){
        setcookie("LOGIN", 'remember' , time()+3600);
        setcookie("USER", $user , time()+3600);
        echo get_file_form();
    }
    else {
        echo "Username and/or password incorrect. Please try again.<br>".get_login_form();
    }
}

//the form of uploading a file
function get_file_form(){
    include('config.php');
    $form = '<h2>Upload the file:</h2>';
    $form .= '<form enctype="multipart/form-data" action="" method="POST">';
    $form .= '<input name="userfile" type="file" />';
    $form .= '<input type="submit" name="upload" value="Upload" /></form>';
    $form .= '<h3 style="margin-left: 50px; margin-top: 50px;">Permitted files:</h3>
              <p>JPG - max size '.$ext["jpg"].'MB </p>
              <p>PDF - max size '.$ext["pdf"].'MB </p>
              <p>ZIP - max size '.$ext["zip"].'MB </p>';
    return $form;
}


//checking what data to show
if($_COOKIE['LOGIN']== 'remember') {

    echo get_file_form();
}
else {

    if ($_POST['submit'] && sizeof($_POST)!==0){

        $user = (isset($_POST['user'])) ? $_POST['user'] : '-';
        $password = (isset($_POST['password'])) ? $_POST['password'] : '-';

        if (mb_strlen($user)>0 && mb_strlen($password)>0){

            login($user, $password);
        }
        else {
            echo "Please fill the form!<br>".get_login_form();

        }
    }
    else {
        echo get_login_form();
    }
}

//if upload botton was pushed, then calling an appropriate function
if(isset( $_POST['upload'])){
     upload_file();
}

//checking the file and uploading
function upload_file() {

    include('config.php');
    $uploaddir = '/var/www/task2/4/'.$_COOKIE['USER'].'/';
    $uploadfile = $uploaddir . $_FILES['userfile']['name'];

    //counting the total size of all files
    $scandir = scandir($uploaddir, 1);
    $max_volume = TOTAL_VOLUME * 1048576;

    foreach($scandir as $file){
        if(strlen($file) > 2){

            $file_size = filesize($uploaddir.$file);

            $volume += $file_size;
        }
    }

    $total_volume = round(($volume / 1048576),2);


    if(file_exists($uploaddir)==false){
        mkdir($uploaddir);
    }

    $file_info = pathinfo($uploadfile);
    $file_ext = $file_info ['extension'];
    $file_size = $_FILES['userfile']['size'];
    $max_size = $ext[$file_ext];

    if(isset($ext[$file_ext]) && $file_size <= ($max_size * 1048576) && ($total_volume + $file_size) <= $max_volume){
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            echo "File was successfully uploaded.\n";
        } else {
            echo "Something went wront. Please try again.";
        }
    }
    else {
        echo "Error! Wrong file extension or too big file size.";
    }

}

//displays the list of user files

$dir = '/var/www/task2/4/'.$_COOKIE['USER'].'/';
$scandir = scandir($dir, 1);

//checking that there files in the dir, not "." and ".."
if ($_COOKIE['LOGIN']== 'remember' && strlen($scandir[0]) > 2) {

    echo '<h3>Your files:</h3><br><table style="border: 1px dotted black; border-collapse: collapse;">';
    foreach($scandir as $file){
        if(strlen($file) > 2){

            $file_size = filesize($dir.$file);
            $volume += $file_size;
            echo '<tr style="border: 1px dotted black;">
                    <td style="border: 1px dotted black; padding: 5px;">'. $file .'</td>
                    <td style="border: 1px dotted black; padding: 5px;">'. round(($file_size / 1048576),4) .' MB</td>
                    <td style="border: 1px dotted black; padding: 5px;"><a href="/task2/4/'. $file .'" download>DOWNLOAD</a></td>
                    <td style="border: 1px dotted black; padding: 5px;"><a href="?delete='. $file .'">DELETE</a></td></tr>';
        }
    }

    $total_volume = round(($volume / 1048576),2);

    $GLOBALS['total_volume'] = $total_volume;

    echo '</table><p>You are using '.$total_volume.' MB from '.TOTAL_VOLUME.' MB </p>';
}


//deleting files
if ($_GET['delete'] && file_exists($dir.$_GET['delete'])) {
    $file = $dir.$_GET['delete'];
    unlink($file);
    //header('Location: index.php');
    // using script because some data have been already output, so the header can't be used
    echo '<script>
                location.reload();
          </script>';
}

