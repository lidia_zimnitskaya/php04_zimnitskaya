<?php

$input_data = array
(
    array('ZZZ', 'BBB', 'CCC'), // Sub-array with strings only!
    array
    (
        array (5, 2, 7), // Sub-array with numbers only!
        array ('C', 56, TRUE), // Combined sub-array.
        array
        (
            array ('C', 'V', 'lalala'), // Sub-array with strings only!
            array (30, 15, 45), // Sub-array with numbers only!
            array (78, 34, 'F') // Combined sub-array.
        )
    ),
    array(12, 95, 1.1, 0.8, 4) // Sub-array with numbers only!
);


function sort_array(&$arr) {

    foreach ($arr as &$value) {
        if ( is_array($value[0]) == false) {
            sort ($value);
        }
        else {
            sort_array ($value);
        }
    }
}
sort_array ($input_data);

print_r ($input_data);
