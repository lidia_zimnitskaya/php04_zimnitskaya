<?php
$input_array = array
(
    array (
        array ('name' => 'Яблоко', 'price' => 10),
        array ('name' => 'Груша',  'price' => 20),
        array ('name' => 'Слива',  'price' => 30)
    ),
    array (
        array ('name' => 'Хлеб',  'price' => 1),
        array ('name' => 'Батон', 'price' => 2),
        array ('name' => 'Булка', 'price' => 3)
    ),
    array (
        array ('name' => 'Минеральная вода',       'price' => 100),
        array ('name' => 'Сок искусственный',      'price' => 200),
        array ('name' => 'Сок натуральный',        'price' => 300),
        array ('name' => 'Напиток энергетический', 'price' => 900)
    )
);
//counting all possible combinations
function combinations(array $data, array &$all = array(), array $group = array(), $value=null, $i = 0)
{
    $keys = array_keys($data);

    if (isset($value) === true) {

        array_push($group, $value);
    }
    if ($i < count($data)) {
        $currentKey     = $keys[$i];
        $currentElement = $data[$currentKey];
        foreach ($currentElement as $val) {
            combinations($data, $all, $group, $val, $i + 1);
        }
    }
    else {
          array_push($all, $group);

    }
return $all;
}
//data outputting
function type_data($data){
    foreach($data as $value){
        foreach($value as $mix){
            $sum+=$mix["price"];
            echo "[".$mix["name"]."]";
        }
        echo " = ".$sum."<br>";
        unset($sum);
    }
}

$result = combinations($input_array);

type_data($result);

