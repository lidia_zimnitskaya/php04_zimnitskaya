<?php

//getting the user_id which we will use
if(isset($_COOKIE['Remember'])) {
    $user_id = $_COOKIE['Remember'];
}
else {
    $user_id = $_SESSION['user_session'];
}

$file = new FILE($DB_con, $user_id);

Class FILE {
    //creating the variables what will need to use working with FILE class
    const DIR = '/var/www/php04_zimnitskaya/task4/2/';

    private $db;
    private $current_user;

    private static $filename;  //the file name for check_file() that will vary depending on the method calling check_file()
    private static $filesize;

    function __construct($DB_con, $user_id)
    {
        $this->db = $DB_con;
        $this->current_user = $user_id;
    }

    public function show_file() { //checkin if there are any files in the user folder

        //getting the info about user files
        $stmt = $this->db->prepare("SELECT * FROM file WHERE user_id=:user_id");

        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $userRow=$stmt->fetchAll(PDO::FETCH_ASSOC);

        //getting an information about user
        $stmt = $this->db->prepare("SELECT user_dir, user_dir_size FROM user WHERE user_id=:user_id");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $userDir=$stmt->fetch(PDO::FETCH_ASSOC);

        //getting total size of uploaded files
        $stmt = $this->db->prepare("SELECT SUM(file_size) AS totalSize FROM file WHERE user_id=:user_id");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $totalSize=$stmt->fetch(PDO::FETCH_ASSOC);

        if(is_array($userRow)) {

            $text = '';
            $text .= '<h2>Your files:</h2><table>';

            foreach ($userRow as $file) {

                $hash_file_name = sha1($file['file_name']);//getting hash name of file

                $text .= '<tr><td>'. $file['file_name'] .'</td>';
                $text .= '<td>'. round($file['file_size']/1048576, 4).' MB</td>';
                $text .= '<td><a href="/php04_zimnitskaya/task4/2/'.$userDir['user_dir'].DIRECTORY_SEPARATOR. $hash_file_name .'" download="'.$file['file_name'].'">DOWNLOAD</a></td>';
                $text .= '<td><a href="?delete='. $file['file_name'] .'">DELETE</a></td></tr>';

            }

                $text .= '</table><p>You are using '. round($totalSize['totalSize']/ 1048576,4) .' MB from '.$userDir['user_dir_size'].' MB </p>';
                echo $text;
        }

    }

    public function get_info() {

        $stmt = $this->db->prepare("SELECT * FROM file_permission WHERE user_id=:user_id");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $userRow=$stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($userRow as $value) {
            $ext = strtoupper($value['permitted_ext']);
            echo "{$ext} - {$value['permitted_size']} MB <br />";
        }

    }


    public function delete_file() {  //deleting the specified file

        //getting an information about user
        $stmt = $this->db->prepare("SELECT user_dir FROM user WHERE user_id=:user_id");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $userDir=$stmt->fetch(PDO::FETCH_ASSOC);

        $directory = self::DIR.$userDir['user_dir'].DIRECTORY_SEPARATOR;

        $file = $directory . $_GET['delete'];
        $file_details = pathinfo($file);
        $hash_file = $directory.sha1($file_details['filename']).".".$file_details['extension'];

        $stmt = $this->db->prepare("DELETE FROM file WHERE user_id=:user_id AND file_name = :file_name LIMIT 1");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user, ':file_name'=>$file_details['basename']));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);
        $queryString = preg_replace('/:file_name/', $file_details['basename'], $stmt->queryString);
        QUERIES::get_queries($queryString, $msc);


        if(file_exists($hash_file)) {
            unlink($hash_file);
        };


    }

    private function check_file() {
        //getting user directory name and permitted directory limit
        $stmt = $this->db->prepare("SELECT user_dir, user_dir_size FROM user WHERE user_id=:user_id");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $userDir=$stmt->fetch(PDO::FETCH_ASSOC);
        $max_volume = $userDir['user_dir_size'] * 1048576; //getting

        $uploaddir = self::DIR.$userDir.DIRECTORY_SEPARATOR;

        $uploadfile = $uploaddir.FILE::$filename;//getting the file name with the path to it

        $file_pathinfo = pathinfo($uploadfile);

        $uploading_file_extension = $file_pathinfo ['extension'];

        //getting user files permissions
        $stmt = $this->db->prepare("SELECT * FROM file_permission WHERE user_id=:user_id");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $file_permissions=$stmt->fetchAll(PDO::FETCH_ASSOC);

        $allowed_files = array();

        foreach($file_permissions as $key=>$value){

            $allowed_files[$value['permitted_ext']]=$value['permitted_size'];

        }
        // getting the total size of uploaded files
        $stmt = $this->db->prepare("SELECT SUM(file_size) AS total_size FROM file WHERE user_id=:user_id");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $total_volume=$stmt->fetch(PDO::FETCH_ASSOC);

        //chekimg if the file meets the requirements (file extention, file limit and directory limit)
        if(isset($allowed_files[$uploading_file_extension]) && FILE::$filesize <= ($allowed_files[$uploading_file_extension]*1048576) && ($total_volume['total_size'] +  FILE::$filesize) <= $max_volume ) {
            return TRUE;
        }
    }

    public function upload_file() {

        //getting user directory name
        $stmt = $this->db->prepare("SELECT user_dir FROM user WHERE user_id=:user_id");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $userDir=$stmt->fetch(PDO::FETCH_ASSOC);

        FILE::$filename = $_FILES['userfile']['name']; // setting the value for $filename to use it in check_file() method
        FILE::$filesize = $_FILES['userfile']['size'];// setting the value for $filesize to use it in check_file() method

        $file_pathinfo = pathinfo($_FILES['userfile']['name']);
        $file_extension = $file_pathinfo['extension'];

        $uploaddir = self::DIR.$userDir['user_dir'].DIRECTORY_SEPARATOR;
        $hash_uploadfile = $uploaddir . sha1($_FILES['userfile']['name']);

        if(file_exists($uploaddir.sha1(FILE::$filename))!=true) {
           if ($this -> check_file()){ // cheking file
                if(file_exists($uploaddir)==false){ // checking if the directory for a user is exist
                    mkdir($uploaddir);
                }
                //moving file to user folder
                if (move_uploaded_file($_FILES['userfile']['tmp_name'], $hash_uploadfile)) {
                    echo "File was successfully uploaded.\n";
                } else {
                   echo "Something went wront. Please try again.";
                }
                //adding the file data to db
               $stmt = $this->db->prepare("INSERT INTO file (file_id, user_id, file_name, file_ext, file_size, file_date)
                                        VALUES (NULL, :user_id, :file_name, :file_extension, :file_size, :file_date)");

               $stmt->execute(array(':user_id'=>$this->current_user, ':file_name'=>FILE::$filename, ':file_extension'=>$file_extension, ':file_size'=>FILE::$filesize, 'file_date' => time() ));

            }

        }
    }

    public function download_to_server() {

        //getting user directory name
        $stmt = $this->db->prepare("SELECT user_dir FROM user WHERE user_id=:user_id");
        $msc = microtime(true); // getting the time before query
        $stmt->execute(array(':user_id'=>$this->current_user));
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $userDir=$stmt->fetch(PDO::FETCH_ASSOC);

        $destination_folder = self::DIR.$userDir['user_dir'].DIRECTORY_SEPARATOR;// setting the value for $filename to use it in check_file() method
        $url = $_POST['url'];

        FILE::$filename = basename($url);
        FILE::$filesize = strlen(file_get_contents($url));// setting the value for $filesize to use it in check_file() method

        error_reporting(0); // if there is no a user folder, php shows an error. Switching it off.
        if(file_exists($destination_folder)==false){ // checking if the directory for a user is exist
            mkdir($destination_folder);
        }

        $file_pathinfo = pathinfo($destination_folder.FILE::$filename);
        $file_extension = $file_pathinfo['extension'];

        $newfname = $destination_folder . sha1(basename($url)); //downloading file to a specified folder

        if(file_exists($uploaddir.sha1(FILE::$filename))!=true) {
            if ($this->check_file()) {
                $file = fopen($url, "rb");
                if ($file) {
                    $newf = fopen($newfname, "wb");
                    if ($newf)
                        while (!feof($file)) {
                            fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                        }
                }
                //adding the file data to db
                $stmt = $this->db->prepare("INSERT INTO file (file_id, user_id, file_name, file_ext, file_size, file_date)
                                            VALUES (NULL, :user_id, :file_name, :file_extension, :file_size, :file_date)");

                $stmt->execute(array(':user_id' => $this->current_user, ':file_name' => FILE::$filename, ':file_extension' => $file_extension, ':file_size' => FILE::$filesize, 'file_date' => time()));

            } else {

                echo "ERROR";
            }
        }
    }

    public function download_to_pc() {

        $destination_folder = self::DIR.'downloads/';// setting the value for $filename to use it in check_file() method

        //error_reporting(0); // if there is no a downloads folder, php shows an error. Switching it off.
        if(file_exists($destination_folder)==false){ // checking if the directory for a user is exist
            mkdir($destination_folder);
        }

        $url = $_POST['url'];

        FILE::$filename = basename($url);
        FILE::$filesize = strlen(file_get_contents($url));// setting the value for $filesize to use it in check_file() method

        set_time_limit(0);
        //This is the file where we save the    information
        $fp = fopen ($destination_folder.basename($url) , 'w+');
//Here is the file we are downloading, replace spaces with %20
        $ch = curl_init(str_replace(" ","%20",$url));
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
// write curl response to file
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
// get curl response
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        header("Content-disposition: attachment;filename=".basename($url));// downloading a file from the server to user PC
        readfile($destination_folder.basename($url));


    }

    public function show_statistics() {

        //counting users
        $stmt = $this->db->prepare("SELECT COUNT(user_id) AS user_num FROM user WHERE 1");
        $msc = microtime(true); // getting the time before query
        $stmt->execute();
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);
        $userQuantity=$stmt->fetch(PDO::FETCH_ASSOC);

        //counting files volume
        $stmt = $this->db->prepare("SELECT SUM(file_size) AS file_volume FROM file WHERE 1");
        $msc = microtime(true); // getting the time before query
        $stmt->execute();
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);

        $fileVolume=$stmt->fetch(PDO::FETCH_ASSOC);
        $fileVolume = round(($fileVolume['file_volume']/1048576), 4);

        //counting files quantity
        $stmt = $this->db->prepare("SELECT COUNT(file_id) AS file_num FROM file WHERE 1");
        $msc = microtime(true); // getting the time before query
        $stmt->execute();
        $msc = microtime(true)-$msc; //calculateing the time passed after the query was finished

        $queryString = preg_replace('/:user_id/', $this->current_user, $stmt->queryString);

        QUERIES::get_queries($queryString, $msc);
        $fileNum=$stmt->fetch(PDO::FETCH_ASSOC);

        //getting an average volume of downloaded files per user
        $average_volume = $fileVolume / $userQuantity["user_num"];

        $text = "";
        $text .= '<h2>Statistics:</h2><table style="text-align: right;">';
        $text .= '<tr><td>Quantity of users</td><td>Total size of downloaded files, MB</td><td>Quantity of downloaded files</td><td>Average volume of downloaded files per user</td></tr>';
        $text .= '<tr><td>'. $userQuantity["user_num"] .'</td>';
        $text .= '<td>'. $fileVolume .'</td>';
        $text .= '<td>'. $fileNum['file_num'] .'</td>';
        $text .= '<td>'. $average_volume .'</td></tr>';
        $text .= '</table>';
        echo $text;
    }

}

