<?php

declare(strict_types=1);

session_start();


$DB_host = "localhost";
$DB_user = "root";
$DB_pass = "123456";
$DB_name = "files_catalog";

try
{
    $DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
    $DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e)
{
    echo $e->getMessage();
}



include_once 'class/class.user.php';
include_once 'class/class.file.php';
include_once 'class/class.db_log.php';
