<?php

$query = new QUERIES($queryArray);

class QUERIES {

    public static $queryArray =array() ;
    const LOGFILE = '/var/www/php04_zimnitskaya/task4/2/class/query.log';

        public function get_queries($queryString, $time) {

           //$content = '<div style="color:red;font-size: 14"><span>'.$queryString.'</span> - <b>'.$time.'</b> s </div>';
            $data[$queryString] = $time;

            file_put_contents(self::LOGFILE, $data, FILE_APPEND);

        }

        //putting text of queries and its execution time to log file
        public function show_log_contents() {

            $log = file_get_contents(self::LOGFILE);
            echo $log;
        }

        //clearing the log file
        public function clear_log_file() {

            file_put_contents(self::LOGFILE, "");
        }
}

