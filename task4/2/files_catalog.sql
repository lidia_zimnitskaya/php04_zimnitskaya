-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 09, 2017 at 05:00 PM
-- Server version: 5.7.18-0ubuntu0.17.04.1
-- PHP Version: 7.0.19-1+deb.sury.org~zesty+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `files_catalog`
--

-- --------------------------------------------------------

--
-- Table structure for table `cookie`
--

CREATE TABLE `cookie` (
  `id_cookie` int(11) NOT NULL,
  `hash_cookie` varchar(200) NOT NULL,
  `time` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cookie`
--

INSERT INTO `cookie` (`id_cookie`, `hash_cookie`, `time`) VALUES
(14, '34b30eb62a74f3b982e1092bf658d14520f1ae1e', 1498225478);

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `file_name` varchar(50) NOT NULL,
  `file_ext` varchar(50) NOT NULL,
  `file_size` int(11) NOT NULL,
  `file_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `user_id`, `file_name`, `file_ext`, `file_size`, `file_date`) VALUES
(15, 2, 'ava.jpg', 'jpg', 553000, 1496789700),
(41, 1, 'Chrysanthemum.jpg', 'jpg', 879394, 1497016537),
(42, 1, 'Tulips - ÐºÐ¾Ð¿Ð¸Ñ.jpg', 'jpg', 620888, 1497016605),
(43, 1, 'Tulips.jpg', 'jpg', 620888, 1497016746);

-- --------------------------------------------------------

--
-- Table structure for table `file_permission`
--

CREATE TABLE `file_permission` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `permitted_ext` varchar(50) NOT NULL,
  `permitted_size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `file_permission`
--

INSERT INTO `file_permission` (`id`, `user_id`, `permitted_ext`, `permitted_size`) VALUES
(1, 2, 'zip', 3),
(2, 2, 'jpg', 2),
(3, 1, 'jpg', 2),
(4, 1, 'pdf', 2);

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_quantity` int(11) NOT NULL,
  `total_files_size` int(11) NOT NULL,
  `avarege_files_size_per_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `login` varchar(50) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(72) NOT NULL,
  `email` varchar(50) NOT NULL,
  `user_dir` varchar(50) NOT NULL,
  `user_dir_size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `login`, `user_name`, `password`, `email`, `user_dir`, `user_dir_size`) VALUES
(1, 'root', 'Den', '$2y$10$BBCpJxgPa8K.iw9ZporxzuW2Lt478RPUV/JFvKRHKzJhIwGhd1tpa', 'root@tut.by', 'root', 15),
(2, 'user1', 'Anna', '$2y$10$BBCpJxgPa8K.iw9ZporxzuW2Lt478RPUV/JFvKRHKzJhIwGhd1tpa', 'den@tut.by', 'user_den', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cookie`
--
ALTER TABLE `cookie`
  ADD PRIMARY KEY (`id_cookie`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`),
  ADD UNIQUE KEY `UNQ_name_user` (`user_id`,`file_name`),
  ADD KEY `IXFK_file_user` (`user_id`),
  ADD KEY `file_name` (`file_name`);

--
-- Indexes for table `file_permission`
--
ALTER TABLE `file_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_file_permission_user` (`user_id`);

--
-- Indexes for table `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `UNQ_user` (`login`,`user_dir`,`email`),
  ADD KEY `user` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cookie`
--
ALTER TABLE `cookie`
  MODIFY `id_cookie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `file_permission`
--
ALTER TABLE `file_permission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `statistics`
--
ALTER TABLE `statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `FK_file_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `file_permission`
--
ALTER TABLE `file_permission`
  ADD CONSTRAINT `FK_file_permission_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
