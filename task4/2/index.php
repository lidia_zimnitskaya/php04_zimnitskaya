<?php

include_once('class/config.class.php');

//checking if the user was logged in before
if($user->is_loggedin()!=""){
    $user->redirect('catalog.php');
}

if ($_POST['submit'] && sizeof($_POST)!==0 && $_POST['remember']!='on'){

    $uname = $_POST['user'] ?? ''; //checking that the inputs not empty
    $upassword = $_POST['password'] ?? '';
    $uname = trim($uname);
    $upassword = trim($upassword);

    if (mb_strlen($uname)>0 && mb_strlen($upassword)>0){
        //logging the user in and redirecting to the page with upload forms and directory
        $user->login($uname, $upassword);
    }

}

if($_POST['submit'] && sizeof($_POST)!==0 && $_POST['remember']=='on') {

    $uname = $_POST['user'] ?? ''; //checking that the inputs not empty
    $upassword = $_POST['password'] ?? '';
    $uname = trim($uname);
    $upassword = trim($upassword);

    if (mb_strlen($uname)>0 && mb_strlen($upassword)>0){
        //
        $user->remember_me($uname, $upassword);
    }

}

?>

<!DOCTYPE html>

<html>
<head>
    <style type="text/css">
        .style {
            padding-bottom: 5px;
            margin-bottom: 3px;
            width: 300px;
        }
        .button {
            padding-bottom: 5px;
            margin-bottom: 3px;
            width: 100px;
        }
    </style>
</head>
<body>

<form action="" method="post" enctype="" style="margin-left: 50px; margin-top: 50px;">
    <input type="text" id="user" name="user" placeholder="User" value=""  class="style"><br>
    <input type="password" name="password" placeholder="Password" value=""  class="style"><br>
    <input type="checkbox" name="remember"> Remember me<br>
    <input type="submit" name="submit" value="Login" class="button" /><br>
</form>


<?

$query->show_log_contents();
$query->clear_log_file();

?>
</body>
</html>


