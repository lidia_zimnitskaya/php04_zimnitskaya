<div class="section">
    <div class="row">
        <div class="auth-string">
            {DB="logged_message"} {UD="user_name"}. <a href="?logout=true">{DB="text_logout"}</a>
        </div>
    </div>
</div>