<div class="col col-3-5 vertical_border">

    <table class="table-file" id="file-table">
        <thead class="thead">
            <tr class="tr-file">
                <td class="th-file sortable asc" data-type="string" id="file-name">
                    {DB="table_file_name"}
                </td>
                <td class="th-file sortable asc" data-type="number" id="file-size">
                    {DB="table_file_size"}
                </td>
                <td class="th-file sortable asc" data-type="string" id="file-date">
                    {DB="table_file_date"}
                </td>
                <td class="th-file">
                    {DB="table_file_action"}
                </td>
            </tr>
        </thead>
        <tbody id="tbody">
            {LOOP=user_files="table_tr.tpl"}
        </tbody>
    </table>

    <div class="subsection-border">
        <h3>{DB="text_upload_title"}:</h3>
        <form enctype="multipart/form-data" action="" method="POST" id="form_upload"  class="fileform">
            <div>
                <label for = "userfile" class="selectbutton" />{DB="text_choose"}
                <input type="file" id="userfile" name="userfile" class="input_upload"/></div>
            <input type="submit" name="upload" class="button upload_button" value="{DB="text_upload_button"}"/>
        </form>

    </div>
    <div style="clear:both;"></div>
    <div class="subsection-border">
        <h3>{DB="text_download_title"}:</h3>
        <form enctype="" action="" method="POST">
            <input name="url" class="input-url" type="text" placeholder="" />
            <div class="margin-space">
                <b>{DB="text_to_store"}: </b>
                <span class="checkbox-file">
                      <input type="checkbox" id="download" name="save_as">
                      <label for="download"></label>
                </span ><br>
            </div>
            <input type="submit" name="download" class="button" value="{DB="text_download_button"}" />
        </form>
    </div>

</div>
