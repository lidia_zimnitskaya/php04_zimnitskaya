<?php


Class FILE {
    //creating the variables what will need to use working with FILE class
    const DIR = '/var/www/php04_zimnitskaya/task5/2/';

    private $db;
    private $current_user;
    private $user_permits;

    private static $filename;  //the file name for check_file() that will vary depending on the method calling check_file()
    private static $filesize;

    function __construct($DB_con, $id, $user_permits, $user_data)
    {
        $this->db = $DB_con;
        $this->current_user = $id;
        $this->user_permits = $user_permits;
        $this->user_data = $user_data;
    }

    public function delete_file() //deleting the specified file
    {
        $userDir=$this->user_data ['user_dir']; // user directory name
        $directory = self::DIR.$userDir.DIRECTORY_SEPARATOR;
        $hash_file = $directory.sha1($_GET['delete']);
        $stmt = $this->db->prepare("DELETE FROM file WHERE user_id=:user_id AND file_name = :file_name LIMIT 1");
        $stmt->execute(array(':user_id'=>$this->current_user, ':file_name'=>$_GET['delete']));

        if(file_exists($hash_file)) {
            unlink($hash_file);
        };
        header("Refresh:0; url=".$_SERVER['PHP_SELF']);
    }

    private function check_file() //: bool
    {
        $userDir=$this->user_data ['user_dir']; // user directory name
        $max_volume = $this->user_data['user_dir_size'] * 1048576; //getting permitted directory limit in bytes
        $uploaddir = self::DIR.$userDir.DIRECTORY_SEPARATOR;
        $uploadfile = $uploaddir.FILE::$filename;//getting the file name with the path to it
        $file_pathinfo = pathinfo($uploadfile);
        $uploading_file_extension = $file_pathinfo ['extension'];
        $total_volume=$this->user_data ['total_files_volume_byt']; // the total size of uploaded files

        //getting user files permissions
        $allowed_files = array();
        foreach($this->user_permits as $key=>$value){
            $allowed_files[$value['permitted_ext']]=$value['permitted_size'];
        }
        //cheking if the file meets the requirements (file extention, file limit and directory limit)
        if(isset($allowed_files[$uploading_file_extension]) && FILE::$filesize <= ($allowed_files[$uploading_file_extension]*1048576)
            && ($total_volume +  FILE::$filesize) <= $max_volume ) {
            return TRUE;
        }
    }

    public function upload_file()
    {

        $userDir = $this->user_data ['user_dir']; // user directory name
        FILE::$filename = $_FILES['userfile']['name']; // setting the value for $filename to use it in check_file() method
        FILE::$filesize = $_FILES['userfile']['size'];// setting the value for $filesize to use it in check_file() method
        $file_pathinfo = pathinfo($_FILES['userfile']['name']);
        $file_extension = $file_pathinfo['extension'];

        $uploaddir = self::DIR.$userDir.DIRECTORY_SEPARATOR;
        $hash_uploadfile = $uploaddir . sha1($_FILES['userfile']['name']);

        if(file_exists($uploaddir.sha1(FILE::$filename))!=true) {
           if ($this -> check_file()){ // cheking file
                if(file_exists($uploaddir)==false){ // checking if the directory for a user is exist
                    mkdir($uploaddir);
                }
                //moving file to user folder
                if (move_uploaded_file($_FILES['userfile']['tmp_name'], $hash_uploadfile)) {
                    //echo "File was successfully uploaded.\n";  //plannig to put config messages one day
                } else {
                  // echo "Something went wront. Please try again.";
                }
                //adding the file data to db
               $stmt = $this->db->prepare("INSERT INTO file (file_id, user_id, file_name, file_ext, file_size, file_date)
                                        VALUES (NULL, :user_id, :file_name, :file_extension, :file_size, :file_date)");

               $stmt->execute(array(':user_id'=>$this->current_user,
                                    ':file_name'=>FILE::$filename,
                                    ':file_extension'=>$file_extension,
                                    ':file_size'=>FILE::$filesize,
                                    'file_date' => time() ));

           }

        }
        header("Refresh:0; url=".$_SERVER['PHP_SELF']);
    }

    public function download_to_server()
    {
        $userDir = $this->user_data ['user_dir']; // user directory name
        $destination_folder = self::DIR.$userDir.DIRECTORY_SEPARATOR;// setting the value for $filename to use it in check_file() method
        $url = $_POST['url'];
        FILE::$filename = basename($url);
        FILE::$filesize = strlen(file_get_contents($url));// setting the value for $filesize to use it in check_file() method

        //error_reporting(0); // if there is no a user folder, php shows an error. Switching it off.
        if(file_exists($destination_folder)==false){ // checking if the directory for a user is exist
            mkdir($destination_folder);
        }

        $file_pathinfo = pathinfo($destination_folder.FILE::$filename);
        $file_extension = $file_pathinfo['extension'];

        $newfname = $destination_folder . sha1(basename($url)); //downloading file to a specified folder

        if(file_exists($destination_folder.sha1(FILE::$filename))!=true) {
            if ($this->check_file()) {
                $file = fopen($url, "rb");
                if ($file) {
                    $newf = fopen($newfname, "wb");
                    if ($newf)
                        while (!feof($file)) {
                            fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                        }
                }
                //adding the file data to db
                $stmt = $this->db->prepare("INSERT INTO file (file_id, user_id, file_name, file_ext, file_size, file_date)
                                            VALUES (NULL, :user_id, :file_name, :file_extension, :file_size, :file_date)");

                $stmt->execute(array(':user_id' => $this->current_user,
                                    ':file_name' => FILE::$filename,
                                    ':file_extension' => $file_extension,
                                    ':file_size' => FILE::$filesize,
                                    'file_date' => time()));
                header("Refresh:0; url=".$_SERVER['PHP_SELF']);

            }
        }
    }

    public function download_to_pc() {

        $destination_folder = self::DIR.'downloads/';// setting the value for $filename to use it in check_file() method

        error_reporting(0); // if there is no a downloads folder, php shows an error. Switching it off.
        if(file_exists($destination_folder)==false){ // checking if the directory for a user is exist
            mkdir($destination_folder);
        }

        $url = $_POST['url'];

        FILE::$filename = basename($url);
        FILE::$filesize = strlen(file_get_contents($url));// setting the value for $filesize to use it in check_file() method

        set_time_limit(0);
        //This is the file where we save the    information
        $fp = fopen ($destination_folder.basename($url) , 'w+');
        //Here is the file we are downloading, replace spaces with %20
        $ch = curl_init(str_replace(" ","%20",$url));
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        // write curl response to file
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        // get curl response
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        header("Content-disposition: attachment;filename=".basename($url));// downloading a file from the server to user PC
        readfile($destination_folder.basename($url));

    }


}

