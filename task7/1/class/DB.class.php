<?php

declare(strict_types=1);

session_start();


try
{
    $DB_con = new PDO('mysql:dbname='.CONFIG_DB_BASE.';port='.CONFIG_DB_PORT.';charset='.CONFIG_DB_CHAR, CONFIG_DB_USER, CONFIG_DB_PASS);
    $DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e)
{
    echo $e->getMessage();
}



class DB
{
    private $db;
    private $parameters;
    private $stat_data; // array to put the stat data in
    private $user_permits; // array to put in the info about user permissions
    private $user_data; // array to put in the info about user
    private $user_files;

    function __construct($DB_con)
    {
        $this->db = $DB_con;
    }
    // getting the text information such as titles and descriptions for our application
    public function get_application_parameters () : array
    {
        $stmt = $this->db->prepare("SELECT * FROM application_parameters");
        $stmt->execute();
        $this->parameters = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $this->parameters;
    }

    public function return_application_parameters () : array
    {
        $this->get_application_parameters();
        return $this->parameters[0];
    }

    //countiong the stat data to show them at the page
    public function get_stat_data() : array
    {
        //counting users
        $stmt = $this->db->prepare("SELECT COUNT(user_id) AS user_num FROM user WHERE 1");
        $stmt->execute();
        $userQuantity=$stmt->fetch(PDO::FETCH_ASSOC);
        $this->stat_data ['users_quantity']= $userQuantity ['user_num'];

        //counting files volume
        $stmt = $this->db->prepare("SELECT SUM(file_size) AS file_volume FROM file WHERE 1");
        $stmt->execute();

        $fileVolume=$stmt->fetch(PDO::FETCH_ASSOC);
        $fileVolume = round(($fileVolume['file_volume']/1048576), 4);
        $this->stat_data ['total_volume']= $fileVolume;

        //counting files quantity
        $stmt = $this->db->prepare("SELECT COUNT(file_id) AS file_num FROM file WHERE 1");
        $stmt->execute();
        $fileNum=$stmt->fetch(PDO::FETCH_ASSOC);
        $this->stat_data ['files_quantity']= $fileNum ['file_num'];

        //countiong the volume of stored files per user
        $this->stat_data ['ratio']= round($fileVolume / $userQuantity ['user_num'], 4);

        return $this->stat_data;
    }

    public function get_user_permits($id) : array
    {
        $stmt = $this->db->prepare("SELECT permitted_ext, permitted_size FROM file_permission WHERE user_id=:id");
        $stmt->execute(array(':id'=>$id));
        $this->user_permits = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $this->user_permits;
    }

    public function get_user_info ($id) : array
    {
        $stmt = $this->db->prepare("SELECT * FROM user WHERE user_id=:id"); //getting the info from table user
        $stmt->execute(array(':id'=>$id));
        $array = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->user_data = $array[0];

        $stmt = $this->db->prepare("SELECT SUM(file_size) AS total_files_volume FROM file WHERE user_id=:id"); //getting the info about the used total files size in the directory
        $stmt->execute(array(':id'=>$id));
        $array = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->user_data['total_files_volume_byt']= $array['total_files_volume'];
        $this->user_data['total_files_volume_mb']= round($array['total_files_volume']/1048576, 2);

        return $this->user_data;
    }

    public function get_user_files($id) //: ?array - not declarating type null or string, it doesn't work in 7.0, works in 7.1
    {
        $stmt = $this->db->prepare("SELECT file_name, file_ext, file_size, file_date FROM file WHERE user_id=:id"); //getting the info from table user
        $stmt->execute(array(':id'=>$id));
        $array = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //adding to an array extra parameters that we need for our table: size in kb, date in necessary format, path to file to download, hash file name to find in directory
        foreach ($array as $item) {
            $item['file_size_kb'] = round( $item ['file_size'] / 1000, 2);
            $date_int=intval($item['file_date']);
            $date = date("d M Y", $date_int);
            $item ['table_date'] = $date;
            $item ['action'] = "Delete";
            $item ['hash_file_name'] = sha1($item ['file_name']);
            $item ['download_path'] = dirname($_SERVER['REQUEST_URI']).DIRECTORY_SEPARATOR.$this->user_data['user_dir'].DIRECTORY_SEPARATOR;
            $this->user_files [] = $item;
        }
        return $this->user_files;
    }
}



