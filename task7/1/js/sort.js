var $tBody = $("#tbody")[0];  //getting the array with only tbody rows

// checking if there are cookies with sort parameters
$(window).on("load", function() {
    if($.cookie('cellIndex') != '' && $.cookie('dataType') != '' && $.cookie('sort') != '') {

        sortTable($.cookie('cellIndex'), $.cookie('dataType'), $.cookie('sort'));

    }
});

//defining the parameters for sorting function
$( ".sortable" ).click(function(event) {

    var $cellIndex = this.cellIndex; // identifying what cell was clicked
    var $dataType = $(this).attr("data-type");

    if(this.className == 'th-file sortable asc') {
        $.cookie('cellIndex', $cellIndex, { expires: 1 }); // setting cookies
        $.cookie('dataType', $dataType, { expires: 1 });
        $.cookie('sort', "1", { expires: 1 });
        sortTable($cellIndex, $dataType, 1);
    }
    else if (this.className == 'th-file sortable desc') {
        $.cookie('cellIndex', $cellIndex, { expires: 1 });// setting cookies
        $.cookie('dataType', $dataType, { expires: 1 });
        $.cookie('sort', "-1", { expires: 1 });
        sortTable($cellIndex, $dataType, -1);
    }

    $(this).toggleClass( 'asc'); // toggle the class asc that identify ascending sorting after click
    $(this).toggleClass( 'desc'); // toggle the class desc that means that after second click sorting will be descending

});


//sorting the table
function sortTable(col, dataType, sort) {

    var rows = $tBody.rows,
        rowlen = rows.length,
        arr = new Array(),
        i, j, cells, celllen;
    // fill the array with cells from the table
    for (i = 0; i < rowlen; i++) {
        cells = rows[i].cells;
        celllen = cells.length;
        arr[i] = new Array();
        for (j = 0; j < celllen; j++) {
            arr[i][j] = $(cells[j]).eq(0).html();

        }
    }

    arr.sort(function (a, b) { // sorting the array by the specified column number (col) and order (sort) depending on data-type

        if (dataType == "number") {
           // console.log(a[col]);
            return (sort == 1) ? (a[col] - b[col]) : (b[col] - a[col]);
        }
        else  {
            return (a[col] == b[col]) ? 0 : ((a[col] > b[col]) ? sort : -1 * sort);
        }
    });
    // replacing existing rows with new rows created from the sorted array
    for (i = 0; i < rowlen; i++) {
        $(rows[i]).eq(0).html("<td class='td-file'>" + arr[i].join("</td><td class='td-file'>") + "</td>");
    }
}

