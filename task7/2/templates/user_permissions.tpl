<div class="section main_section">
    <div class="row work_area">
        <div class="col col-1-5">
            <div class="information">
                <strong>{DB="text_file_permit"}:</strong><br>
                {LOOP=user_permits="files_permissions.tpl"}
                <br>
                <strong>{DB="text_used_volume"}:</strong><br>
                <span id="used_volume">{UD="total_files_volume_mb"}</span> Mb from {UD="user_dir_size"} Mb
            </div>
        </div>