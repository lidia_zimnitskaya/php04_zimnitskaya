<div class="section main_section">
    <div class="row work_area">
        <div class="col col-1-5">
        </div> 
<div class="col col-3-5 vertical_border">
            <form action="" method="post" enctype="">
                <div class="table">
                    <div class="tr">
                        <div class="td_text">
                            {DB="login_text"}:
                        </div>
                        <div class="td">
                            <input type="text" class="input" name="user" value="">
                        </div>
                    </div>
                    <div class="tr">
                        <div class="td_text">
                            {DB="password_text"}:
                        </div>
                        <div class="td">
                            <input type="password" class="input" name="password" value="">
                        </div>
                    </div>
                    <div class="tr">
                        <div class="td_text">
                            {DB="remember_text"}:
                        </div>
                        <div class="td">
                            <div class="checkbox">
                                <input type="checkbox" id="login-check" name="remember">
                                <label for="login-check"></label>
                            </div >
                        </div>
                    </div>
                    <div class="tr">
                        <div class="td_text">

                        </div>
                        <div class="td">
                            <input type="submit" class="button" name="submit" value="{DB="login_button"}" />
                        </div>
                    </div>
                </div>
            </form>
     <div style="clear:both;"></div>
</div>
