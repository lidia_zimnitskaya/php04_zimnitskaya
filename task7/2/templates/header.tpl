<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="styles/style.css" rel="stylesheet">

    <title>{DB="title"}</title>
</head>
<body>
<div class="section">
    <div class="row">
        <h1>{DB="title"}</h1>
    </div>
</div>
