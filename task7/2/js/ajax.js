
var $tableBody = $("tbody")[0]; // getting the table data
var handler = "handler/ajax.php";

$('tbody').on('click', '.delete', function(event) {
var fileToDel = this.id;
var td = $(this).eq(0).parent(); //getting the parent cell of the span
var bgColor = $(td).eq(0).parent();
bgColor.attr("id", "red");

$('#red').animate({ backgroundColor: "#FA8072" }, 1000, null, function () {

event.preventDefault(); // prevent browser default actions
    // Sending the request
    var request = $.ajax({
        url: handler,
        type: "post",
        data:  ({fileName : fileToDel, delete :"delete", tpl : "table_loop.tpl"}) // sending the array with parameters
    });
    //  if success
    request.done(function (response, textStatus, jqXHR){
        $("#tbody").eq(0).html(response); // reloading the tbody of a table
        undateStatData(); // updating stat data and volume used by user
        undateUsedVolume();
    });
    // if fail
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });

	});
});

$('#form_upload').on('click', '#upload', function(event) {

    event.preventDefault(); // prevent browser default actions

    var file = $("#userfile").prop('files')[0];
    var fileData = new FormData();
    fileData.append("userfile", file);
    fileData.append("tpl", "table_loop.tpl");

    // Sending the request
    var request = $.ajax({
         url: handler, // point to server-side PHP script
         dataType: 'text',  // what to expect back from the PHP script, if anything
         cache: false,
         contentType: false,
         processData: false,
         data: fileData,
         type: 'post', // sending the array with parameters
    });
	var rowCountBefore = $("#tbody").eq(0).children().length;  // count the quantity of files before uploading
	
    //  if success
    request.done(function (response, textStatus, jqXHR){
        $("#tbody").eq(0).html(response); // reloading the tbody of a table
        $("#userfile").eq(0).val("");
        undateStatData(); // updating stat data and volume used by user
        undateUsedVolume();
		var rowCountAfter = $("#tbody").eq(0).children().length;  //count the quantity of files after uploading
		
		//comparing them, if a new row was added , then highlight it
		if (rowCountBefore < rowCountAfter) {
			var lastChild = $("tr").last()[0]; // define the last row where will be the last updated file
			$(lastChild).animate({ backgroundColor: "#3CB371" }, 1000, null)
			$(lastChild).animate({ backgroundColor: "#ffffff" }, 1000, null)
		}

    });
});


$('#form_download').on('click', '.download_button', function(event) {

    event.preventDefault(); // prevent browser default actions

    var url = $("#url-input").val();
    var checked = $("#download").prop( "checked" );

    var fileData = new FormData();
    fileData.append("url", url);
    fileData.append("checked", checked);
    fileData.append("tpl", "table_loop.tpl");

        // Sending the request
        var request = $.ajax({
            url: handler,
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: fileData, // sending the array with parameters
            type: "post"
        });
	var rowCountBefore = $("#tbody").eq(0).children().length;  // count the quantity of files before uploading
    if(checked == true) {
        //  if success
        request.done(function (response, textStatus, jqXHR){
            $("#tbody").eq(0).html(response); // reloading the tbody of a table
            undateStatData(); // updating stat data and volume used by user
            undateUsedVolume();
            $("#url-input").eq(0).val("");
			
		var rowCountAfter = $("#tbody").eq(0).children().length;  //count the quantity of files after uploading
		
		//comparing them, if a new row was added , then highlight it
		if (rowCountBefore < rowCountAfter) {
			var lastChild = $("tr").last()[0]; // define the last row where will be the last updated file
			$(lastChild).animate({ backgroundColor: "#3CB371" }, 1000, null)
			$(lastChild).animate({ backgroundColor: "#ffffff" }, 1000, null)
		}			
		
        });
    }
    else if (checked == false) {

        request.done(function (response, textStatus, jqXHR){
            var link = $("<a href='"+response+"' download></a>");
            link[0].click();
            $("#url-input").eq(0).val("");
        });
    }

});


//updating stat data
function undateStatData() {

    event.preventDefault(); // prevent browser default actions

    var request = $.ajax({
        url: handler,
        type: "post",
        data:  ({tpl : "statistic.tpl"}) // sending the array with parameters
    });

    request.done(function (response, textStatus, jqXHR){
        $("#statistics").eq(0).html(response); // reloading the tbody of a table
    });

}

//update the user used volume in the directory
function undateUsedVolume() {

    event.preventDefault(); // prevent browser default actions

    var request = $.ajax({
        url: handler,
        type: "post",
        data:  ({tpl : "used_volume.tpl"}) // sending the array with parameters
    });

    request.done(function (response, textStatus, jqXHR){
        $("#used_volume").eq(0).html(response); // reloading the tbody of a table
    });
}