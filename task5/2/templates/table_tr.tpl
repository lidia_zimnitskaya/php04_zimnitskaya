<div class="tr-file">
    <div class="td-file">
        <a href="{LD="download_path"}{LD="hash_file_name"}"  download="{LD="file_name"}">{LD="file_name"}</a>
    </div>
    <div class="td-file">
        {LD="file_size_kb"}
    </div>
    <div class="td-file">
        {LD="table_date"}
    </div>
    <div class="td-file">
        <a href="?delete={LD="file_name"}" class="delete">{LD="action"}</a>
    </div>
</div>