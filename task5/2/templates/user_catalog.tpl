<div class="col col-3-5 vertical_border">

    <div class="table-file">
        <div class="thead">
            <div class="tr-file">
                <div class="th-file">
                    {DB="table_file_name"}
                </div>
                <div class="th-file">
                    {DB="table_file_size"}
                </div>
                <div class="th-file">
                    {DB="table_file_date"}
                </div>
                <div class="th-file">
                    {DB="table_file_action"}
                </div>
            </div>
        </div>
        <div class="tbody">
            {LOOP=user_files="table_tr.tpl"}
        </div>
    </div>

    <div class="subsection-border">
        <h3>{DB="text_upload_title"}:</h3>
        <form enctype="multipart/form-data" action="" method="POST" id="form_upload"  class="fileform">
            <div>
                <label for = "userfile" class="selectbutton" />{DB="text_choose"}
                <input type="file" id="userfile" name="userfile" class="input_upload"/></div>
            <input type="submit" name="upload" class="button upload_button" value="{DB="text_upload_button"}"/>
        </form>

    </div>
    <div style="clear:both;"></div>
    <div class="subsection-border">
        <h3>{DB="text_download_title"}:</h3>
        <form enctype="" action="" method="POST">
            <input name="url" class="input-url" type="text" placeholder="" />
            <div class="margin-space">
                <b>{DB="text_to_store"}: </b>
                <span class="checkbox-file">
                      <input type="checkbox" id="download" name="save_as">
                      <label for="download"></label>
                </span ><br>
            </div>
            <input type="submit" name="download" class="button" value="{DB="text_download_button"}" />
        </form>
    </div>

</div>
