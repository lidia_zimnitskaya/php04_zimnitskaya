<?php


Class USER {

    private $db;

    function __construct($DB_con)
    {
        $this->db = $DB_con;
    }

    public function login($uname, $upassword)
    {

        $stmt = $this->db->prepare("SELECT * FROM user WHERE login=:uname LIMIT 1");
        $stmt->execute(array(':uname'=>$uname));
        $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

        if($stmt->rowCount() > 0)
        {
            if(password_verify($upassword, $userRow['password']))
            {
                $_SESSION['user_session'] = $userRow['user_id']; // setting $_sesssion to get the file catalog
                header("Refresh:0; url=".$_SERVER['PHP_SELF']);
            }
            else {
                echo "wrong password!";
            }

        }
    }

    public function remember_me($uname, $upassword)
    {

        $stmt = $this->db->prepare("SELECT * FROM user WHERE login=:uname LIMIT 1");
        $stmt->execute(array(':uname'=>$uname));

        $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

        if($stmt->rowCount() > 0)
        {
            if(password_verify($upassword, $userRow['password']))
            {
                $stmt = $this->db->prepare("INSERT INTO cookie(id_cookie, hash_cookie, time) VALUES (NULL ,:hash, :time)");
                $stmt->execute(array(':hash'=>sha1($_SERVER['REMOTE_ADDR'].$userRow['user_id']), ':time'=>time()+1209600));
                setcookie("Remember", $userRow['user_id'], time()+1209600);// setting $_cookie to make a long-time authorization
                header("Refresh:0; url=".$_SERVER['PHP_SELF']);
            }
            else {
                echo "wrong password!";
            }

        }
    }

    public function is_loggedin()
    {

        $stmt = $this->db->prepare("SELECT time FROM cookie WHERE hash_cookie=:hash");
        $stmt->execute(array(':hash'=>sha1($_SERVER['REMOTE_ADDR'].$_COOKIE['Remember'])));

        $userCookie=$stmt->fetch(PDO::FETCH_ASSOC);
        $cookie_time =$userCookie['time'];
        $current_time = time();

        if(isset($_SESSION['user_session']) || $current_time <= $cookie_time) //checking if the user was logged in before
        {
            return true;
        }
    }



    public function logout()
    {
        session_destroy();
        $stmt = $this->db->prepare("DELETE FROM cookie WHERE hash_cookie=:hash");
        $stmt->execute(array(':hash'=>sha1($_SERVER['REMOTE_ADDR'].$_COOKIE['Remember'])));
        unset($_SESSION['user_session']);
        unset($_COOKIE['Remember']);         // unsetting the variables to log out
        setcookie("Remember", "", time()-1209600);
        header("Refresh:0; url=".$_SERVER['PHP_SELF']);
        return true;
    }
}
