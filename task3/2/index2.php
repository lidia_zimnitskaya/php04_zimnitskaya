<?php
// checking names
$names = array(
    "bванов Иван Иванович",
    "Ivanov-Ivanov Ivanov Ivanov",
    " Иванов Иван Иванович ",
    "Снова'Иванов Иван Иванович",
    "Ivanov Ivanov Ivanov",
    "Иванов - Иванов Иван Иванович",
    "Ivanov Иван Ivanович",
    "Иванов    Иван Иванович"

);

foreach( $names as $item ) {

    $check = preg_match("/^[\w][\w\-\'\s]{0,60}$/u",$item, $matches);

    if ($check) {

        echo $matches[0].'<br/>';

    }
}