<?php

include_once('service.php');

//checking if the user was logged in before
if($user->is_loggedin()!=""){
    $user->redirect('directory.php');
}

if ($_POST['submit'] && sizeof($_POST)!==0){

    $uname = $_POST['user'] ?? ''; //checking that the inputs not empty
    $upassword = $_POST['password'] ?? '';

    if (mb_strlen($uname)>0 && mb_strlen($upassword)>0){
        //logging the user in and redirecting to the page with upload forms and directory
        $user->login ($uname, $upassword);
        $user->redirect('directory.php');
    }
    else {
        echo "Please fill the form!<br>";

    }
}
?>

<!DOCTYPE html>

<html>
    <head>

    </head>
    <body>

        <form action="" method="post" enctype="" style="margin-left: 50px; margin-top: 50px;">
            <input type="text" id="user" name="user" placeholder="User" value="" style="margin-right: 5px;">
            <input type="password" name="password" placeholder="Password" value="" style="margin-right: 10px;">
            <input type="submit" name="submit" value="Login" />
        </form>

    </body>
</html>