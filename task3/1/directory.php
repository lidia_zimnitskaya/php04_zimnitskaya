<?php

include_once('service.php');

//checking if the user was logged in before
if(!$user->is_loggedin()) {
    $user->redirect('index.php');
}
//unseting the $_session variable and redirecting to the login form
if($_GET['logout']) {
    $user->logout();
    $user->redirect('index.php');
}

if($_POST['upload']) {
        $file -> upload_file();
}

if($_GET['delete']) {
    $file->delete_file();
    $user->redirect('directory.php');
}

if (isset($_POST['download']) && isset($_POST['url'])){
        $file -> download_to_server();
}
if (isset($_POST['save_as']) && isset($_POST['url'])){
    $file -> download_to_pc();
}

?>

<html>
<head>

</head>
<body>

    <h2>Upload the file:</h2>

    <form enctype="multipart/form-data" action="" method="POST">
        <input name="userfile" type="file" style="width: 355px; margin-right: 10px;"/>
        <input type="submit" name="upload" value="Upload" />
    </form>

    <h3>or</h3>

    <form enctype="" action="" method="POST">
        <input name="url" type="text" placeholder="URL" style="width: 355px; margin-right: 10px;"/>
        <input type="submit" name="download" value="Download" />
        <input type="submit" name="save_as" value="Save as" />
    </form>

    <h2>Permitted files:</h2>
    <!--showing what files types user is permitted to add-->
    <?php foreach($user_data['allowedFile'] as $key=>$value) {
    echo "{$key} - {$value} MB <br />";
    }
    //checkin if there are any files in the user folder, if they are, shownig them

    if($file -> scanningdir() > 2):?>
    <h2>Your files:</h2>

    <table style="border: 1px dotted black; border-collapse: collapse;">
    <?$file -> show_file()?>

    <?endif;?>

<p><a href="?logout=true">Logout</a></label></p>

</body>
</html>

