<?php

session_start();

define('XML', 'config.xml');

//Interpreting an XML config file and putting the user data into an array
function get_data_from_xml($user_name, $xml_file = XML, array &$all = array()) {

    $xml_data = simplexml_load_file($xml_file);
    $json_data = json_encode($xml_data);
    $xml_array = json_decode($json_data, TRUE);

    foreach ($xml_array['user'] as $user_data) {

        if($user_data['login'] == $user_name ) {  //looking for information what belongs to a specified user

            array_push($all, $user_data);

        }
    }
return $all[0];

}

//Calling the function to interpret the XML config file
$user_data=get_data_from_xml($_SESSION['user_session']);

//creating new objects and sending arguments with user data to FILE class
$user = new USER;
$file = new FILE($user_data['login'], $user_data['dirLimit'],$user_data['allowedFile']);



Class USER {

    public function login($uname, $upassword){

        $uname = trim($uname);
        $upassword = trim($upassword);
        $check_upass=get_data_from_xml($uname); // getting the users data according to his login name
        if ($check_upass['password'] == $upassword) { //checking if the inputted passford fits its login

            $_SESSION['user_session'] = $uname;  //setting the $_session variable to keep a user logged in
            return true;
        }
        else {
           echo 'Wrong login of password.';
        }


    }

    public function is_loggedin(){

        if(isset($_SESSION['user_session'])) //checking if the user was logged in before
        {
            return true;
        }
    }

    public function redirect($url){

        header("Location: $url");   //redirectingthe user to loging page or to main page
    }

    public function logout(){

        session_destroy();
        unset($_SESSION['user_session']);  // unsetting the $_session variable to log out
        return true;
    }
}

Class FILE {
    //creating the variables what will need to use working with FILE class
    const DIR = '/var/www/php04_zimnitskaya/task3/1/';
    private $name;
    private $dir_limit;
    private $allowed_file;
    private static $filename;  //the file name for check_file() that will vary depending on the method calling check_file()
    private static $filesize;

    function  __construct($name, $dir_limit, $allowed_file) {
        $this -> name = $name;
        $this -> dir_limit = $dir_limit;
        $this -> allowed_file = $allowed_file;
    }
    //method that allows us to check if the file fits to user permitions
    private function check_file() {

        $uploaddir = self::DIR.$this->name.'/';
        $uploadfile = $uploaddir.FILE::$filename;  //getting the file name with the path to it
        $file_info = pathinfo($uploadfile);
        $permitted_info = $this -> allowed_file; //getting permitted file extentions

        print_r($permitted_info);
        $file_ext = $file_info ['extension'];
        $file_size = FILE::$filesize;
        $scandir = scandir($uploaddir, 1);
        $max_volume = $this -> dir_limit * 1048576; //getting permitted directory limit

        foreach($scandir as $file){
            if(strlen($file) > 2){

                $file_size_ = filesize($uploaddir.$file);

                $volume += $file_size_;  //getting the total size of all files in the user directory
            }
        }
        //chekimg if the file meets the requirements
        if(isset($permitted_info[$file_ext]) && $file_size <= ($permitted_info[$file_ext]*1048576) && ($volume +  $file_size) <= $max_volume ) {
            return TRUE;
        }
    }

    public function upload_file() {

        error_reporting(0); // if there is no a user folder, php shows an error. Switching it off.
        FILE::$filename = $_FILES['userfile']['name']; // setting the value for $filename to use it in check_file() method
        FILE::$filesize = $_FILES['userfile']['size'];// setting the value for $filesize to use it in check_file() method
        $uploaddir = self::DIR.$this->name.'/';
        $uploadfile = $uploaddir . $_FILES['userfile']['name'];
        if ($this -> check_file()){ // cheking file
            if(file_exists($uploaddir)==false){ // checking if the directory for a user is exist
                mkdir($uploaddir);
            }
            //moving file to user folder
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
                echo "File was successfully uploaded.\n";
            } else {
                echo "Something went wront. Please try again.";
            }
        }
    }

    public function scanningdir() { //checkin if there are any files in the user folder
        error_reporting(0);
        $dir = self::DIR.$this->name.'/';
        $scandir = scandir($dir, 1);
        $scandir = count($scandir);
        return $scandir;
    }

    public function show_file() { //showning the files that are kept in user folder
        error_reporting(0);
        $dir = self::DIR.$this->name.'/';
        $scandir = scandir($dir, 1);
        foreach($scandir as $file) {

           if(strlen($file) > 2){

                $file_size = round((filesize($dir.$file) / 1048576),4); // getiing the file size, turnig bytes into MB
                $volume += $file_size;

                echo '<tr style="border: 1px dotted black;">
                        <td style="border: 1px dotted black; padding: 5px;">'. $file .'</td>
                        <td style="border: 1px dotted black; padding: 5px;">'. $file_size .' MB</td>
                        <td style="border: 1px dotted black; padding: 5px;"><a href="'.$dir.$file .'" download>DOWNLOAD</a></td>
                        <td style="border: 1px dotted black; padding: 5px;"><a href="?delete='. $file .'">DELETE</a></td></tr>';

            }
        }

       echo '</table><p>You are using '.$volume.' MB from '.$this -> dir_limit.' MB </p>';  //showning to user the size of uploaded files and his limit
    }

    public function delete_file() {  //deleting the specified file
        $dir = self::DIR.$this->name.'/';
        $file = $dir.$_GET['delete'];
        unlink($file);
    }

    public function download_to_server() {

        $destination_folder = self::DIR.$this->name.'/';// setting the value for $filename to use it in check_file() method
        $url = $_POST['url'];

        FILE::$filename = basename($url);
        FILE::$filesize = strlen(file_get_contents($url));// setting the value for $filesize to use it in check_file() method

        error_reporting(0); // if there is no a user folder, php shows an error. Switching it off.
        if(file_exists($destination_folder)==false){ // checking if the directory for a user is exist
            mkdir($destination_folder);
        }

        $newfname = $destination_folder . basename($url); //downloading file to a specified folder
        if ($this -> check_file()){
            $file = fopen ($url, "rb");
            if ($file) {
                $newf = fopen ($newfname, "wb");

                if ($newf)
                    while(!feof($file)) {
                        fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
                    }
            }
        }
        else{
            echo "ERROR";
        }
    }

    public function download_to_pc() {

        $destination_folder = self::DIR.'downloads/';// setting the value for $filename to use it in check_file() method

        error_reporting(0); // if there is no a downloads folder, php shows an error. Switching it off.
        if(file_exists($destination_folder)==false){ // checking if the directory for a user is exist
            mkdir($destination_folder);
        }

        $url = $_POST['url'];

        FILE::$filename = basename($url);
        FILE::$filesize = strlen(file_get_contents($url));// setting the value for $filesize to use it in check_file() method

        $newfname = $destination_folder . basename($url);//downloading file to a specified folder
        if ($this -> check_file()){
            $file = fopen ($url, "rb");
            if ($file) {
                $newf = fopen ($newfname, "wb");

                if ($newf)
                    while(!feof($file)) {
                        fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
                    }
            }
        }
        header("Content-disposition: attachment;filename=".$newfname);// downloading a file from the server to user PC
        readfile($newfname);

    }
}



